
<?php
// Include subtoolbar
// $toolbar_sub = 'views/moderation/_subtoolbar-moderation-listing.php';

// Setup breadcrumbs
$breadcrumbs = array('Moderation', 'Listings', 'Edit');

include 'views/templates/head.php';
?>

<div class="container">
    <div class="grid">
        <div class="grid__item  two-thirds">
            <div class="panel">
                <h6 class="panel__head  text--semibold">Ad Details</h6>
                <div class="panel__body">
                    <div class="list-data  list-data--grids  list-data--form">
                        <dl>
                            <dt>Title</dt>
                            <dd>
                                <input type="text" class="input" value="2015 Subaru Impreza 2.5 WRX STi Hatchback">
                            </dd>
                        </dl>

                        <div class="grid  grid--md">
                            <div class="grid__item  one-third  push-md--ends">
                                <dl>
                                    <dt>Year</dt>
                                    <dd>
                                        <div class="selectize--disable-input  selectize--dropdown-full">
                                            <select id="select-year" class="select  js-selectize">
                                                <option>2017</option>
                                                <option>2016</option>
                                                <option>2015</option>
                                                <option>2014</option>
                                                <option>2013</option>
                                                <option selected>2012</option>
                                                <option>2011</option>
                                                <option>2010</option>
                                            </select>
                                        </div>
                                    </dd>
                                </dl>
                            </div>

                            <div class="grid__item  two-thirds  push-md--ends">
                                <dl>
                                    <dt>Price</dt>
                                    <dd>
                                        <input type="text" class="input" value="RM 130,000">
                                    </dd>
                                </dl>
                            </div>
                        </div>

                        <dl>
                            <dt>Description</dt>
                            <dd>
                                <textarea rows="15" class="input">Selling this beauty machine to upgrade for my family. Honestly its a great car for both worlds, sport/comfort.

                                    * SPORT RIM
                                    * ORIGINAL PAINT
                                    * READY TO DRIVE
                                    * MUST VIEW
                                    * TIP TOP CONDITION
                                    * LIKE NEW
                                    * WELL MAINTAINED
                                    * CLEAN INTERIOR
                                    * SMOOTH ENGINE AND GEARBOX
                                    * GOOD TYRE CONDITION
                                    * SEE TO BELIEVE

                                    - Best price in town!
                                    - Last batch of Version 10 (hatchback).
                                    - New set of tyres.
                                    - Very good condition, super good!
                                    - Paddleshift with auto blip downshift.
                                    - Cruise control.
                                    - Full Leather Seats.
                                    - Start/stop button, Keyless entry.
                                    - Full specs!

                                    Cash buyer can nego a bit more. Serious buyer only.Excellent condition</textarea>
                            </dd>
                        </dl>

                        <div class="grid  grid--md">
                            <div class="grid__item  one-half  push-md--top">
                                <dl>
                                    <dt>Mileage Range <small>(Required)</small></dt>
                                    <dd>
                                        <div class="selectize--disable-input  selectize--dropdown-right  selectize--dropdown-full">
                                            <select id="select-beast" class="select  js-selectize">
                                                <option>2017</option>
                                                <option>2016</option>
                                                <option>2015</option>
                                                <option>2014</option>
                                                <option>2013</option>
                                                <option selected>2012</option>
                                                <option>2011</option>
                                                <option>2010</option>
                                            </select>
                                        </div>
                                    </dd>
                                </dl>
                            </div>

                            <div class="grid__item  one-half  push-md--top">
                                <dl>
                                    <dt>Mileage</dt>
                                    <dd>
                                        <input type="text" class="input" value="RM 130,000">
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end: .panel -->

            <div class="panel">
                <h6 class="panel__head  text--semibold">Location</h6>
                <div class="panel__body">
                    <div class="list-data  list-data--grids  list-data--form">
                        <div class="grid  grid--md">
                            <div class="grid__item  one-third">
                                <dl>
                                    <dt>State <small>(Required)</small></dt>
                                    <dd>
                                        <div class="selectize--disable-input  selectize--dropdown-full">
                                            <select id="select-state" class="select  js-selectize">
                                                <option>2017</option>
                                                <option>2016</option>
                                                <option>2015</option>
                                                <option>2014</option>
                                                <option>2013</option>
                                                <option selected>Selangor</option>
                                                <option>2011</option>
                                                <option>2010</option>
                                            </select>
                                        </div>
                                    </dd>
                                </dl>
                            </div>

                            <div class="grid__item  two-thirds">
                                <dl>
                                    <dt>Area</dt>
                                    <dd>
                                        <div class="selectize--disable-input  selectize--dropdown-full">
                                            <select id="select-area" class="select  js-selectize">
                                                <option>2017</option>
                                                <option>2016</option>
                                                <option>2015</option>
                                                <option>2014</option>
                                                <option>2013</option>
                                                <option selected>Kota Damansara</option>
                                                <option>2011</option>
                                                <option>2010</option>
                                            </select>
                                        </div>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end: .panel -->

            <div class="panel">
                <h6 class="panel__head  text--semibold">Color</h6>
                <div class="panel__body  hard--bottom">
                    <div class="grid  text--center  milli  colors  push-xs--top">
                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-white" name="color">
                            <label class="block" for="color-white">
                                <div class="colors__fill" style="background-color: #FFF;"></div>
                                <div class="colors__label  block-inline  push-xs--top">White</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-black" name="color" checked>
                            <label class="block" for="color-black">
                                <div class="colors__fill" style="background-color: #222;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Black</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-silver" name="color">
                            <label class="block" for="color-silver">
                                <div class="colors__fill" style="background-color: #ccc;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Silver</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-grey" name="color">
                            <label class="block" for="color-grey">
                                <div class="colors__fill" style="background-color: #888;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Grey</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-red" name="color">
                            <label class="block" for="color-red">
                                <div class="colors__fill" style="background-color: #D00;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Red</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-maroon" name="color">
                            <label class="block" for="color-maroon">
                                <div class="colors__fill" style="background-color: #A00;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Maroon</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-bronze" name="color">
                            <label class="block" for="color-bronze">
                                <div class="colors__fill" style="background-color: #CB7D1C;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Bronze</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-blue" name="color">
                            <label class="block" for="color-blue">
                                <div class="colors__fill" style="background-color: #229EE3;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Blue</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-beige" name="color">
                            <label class="block" for="color-beige">
                                <div class="colors__fill" style="background-color: #F5F1DE;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Beige</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-brown" name="color">
                            <label class="block" for="color-brown">
                                <div class="colors__fill" style="background-color: #643300;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Brown</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-gold" name="color">
                            <label class="block" for="color-gold">
                                <div class="colors__fill" style="background-color: #E9BF00;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Gold</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-yellow" name="color">
                            <label class="block" for="color-yellow">
                                <div class="colors__fill" style="background-color: #FEEA00;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Yellow</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-green" name="color">
                            <label class="block" for="color-green">
                                <div class="colors__fill" style="background-color: #51B22D;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Green</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-orange" name="color">
                            <label class="block" for="color-orange">
                                <div class="colors__fill" style="background-color: #FEA400;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Orange</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-magenta" name="color">
                            <label class="block" for="color-magenta">
                                <div class="colors__fill" style="background-color: #DF2A9A;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Magenta</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-purple" name="color">
                            <label class="block" for="color-purple">
                                <div class="colors__fill" style="background-color: #7E1A86;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Purple</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-pink" name="color">
                            <label class="block" for="color-pink">
                                <div class="colors__fill" style="background-color: #FD6FBA;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Pink</div>
                            </label>
                        </div>

                        <div class="grid__item  one-sixth  push-lg--bottom">
                            <input type="radio" id="color-other" name="color">
                            <label class="block" for="color-other">
                                <div class="colors__fill" style="background-color: #FFF;"></div>
                                <div class="colors__label  block-inline  push-xs--top">Other</div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <!--end: .panel -->

            <div class="panel">
                <div class="panel__head">
                    <h6 class="text--semibold  push-sm--bottom">Photos</h6>
                    <div class="milli">Upload up to 21 photos for your ad. Start with your main photo as the first photo will be your main photo. Ads with more than 4 photos usually get the most response. A good mix of exterior and interior shots of your vehicle helps to sell it faster. Buyers want to see your vehicle from all angles. Maximum 12mb per photo Allowed formats: .jpg, .jpeg</div>
                </div>
                <div class="fill--shade  soft-md  text--center">
                    <input type="button" class="btn  btn--primary" value="Upload More Photos">
                </div>
                <div class="panel__body  hard--top">
                    <div class="grid  grid--md">
                        <?php for($count = 1; $count < 19; $count++) { ?>
                        <div class="grid__item  one-quarter  push-md--top">
                            <img src="images/gallery/subaru-<?=$count?>.jpeg" class="block">
                            <div class="clearfix  milli  fill--shade  soft-sm">
                                <?php if($count == 1) {?>
                                    <div class="text--muted  float--left">Main Photo</div>
                                <?php } else { ?>
                                    <a href="#" class="float--left">Set as Main</a>
                                <?php } ?>
                                <a href="#" class="float--right">Remove</a>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!--end: .panel -->

            <div class="clearfix">
                <div class="push-md--ends">Note: For Post-Moderation, Update will also approve the listing.</div>
                <hr class="rule  rule--light  push-md--ends">

                <input type="button" class="btn  btn--primary  float--right" value="Update Listing">
                <input type="button" class="btn  btn--positive  float--left" value="Approve">
                <input type="button" class="btn  float--left  push-sm--left" value="Reject">
            </div>

            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </div>
        <!--end: .grid__item.two-thirds -->
        <div class="grid__item  one-third">
            <div class="panel">
                <h6 class="panel__head  text--semibold">Seller Details</h6>
                <div class="panel__body">
                    <div class="list-data">
                        <dl>
                            <dt>Trusted Dealer</dt>
                            <dd><span class="text--positive">True</span></dd>
                        </dl>

                        <dl>
                            <dt>Company</dt>
                            <dd>Damansara Automotive Sdn Bhd</dd>
                        </dl>

                        <dl>
                            <dt>Mobile Number</dt>
                            <dd>+6012-3456789</dd>
                        </dl>

                        <dl>
                            <dt>User Name</dt>
                            <dd>damansaraauto</dd>
                        </dl>

                        <dl>
                            <dt>Person Name</dt>
                            <dd>Parveen Kumar</dd>
                        </dl>

                        <dl>
                            <dt>Email</dt>
                            <dd>parveen.kumar@icarasia.com</dd>
                        </dl>

                        <dl>
                            <dt>Package</dt>
                            <dd>Silver - 12 Months (Exp Dec 31, 2017)</dd>
                        </dl>
                    </div>
                </div>
            </div>
            <!--end: .panel -->

            <div class="panel">
                <h6 class="panel__head  text--semibold">Vehicle Details</h6>
                <div class="panel__body">
                    <div class="list-data">
                        <dl>
                            <dt>Make</dt>
                            <dd>Subaru</dd>
                        </dl>

                        <dl>
                            <dt>Model</dt>
                            <dd>Impreza</dd>
                        </dl>

                        <dl>
                            <dt>Year</dt>
                            <dd>2015</dd>
                        </dl>

                        <dl>
                            <dt>Variant</dt>
                            <dd>2.0 WRX STi S206</dd>
                        </dl>

                        <dl>
                            <dt>Nickname</dt>
                            <dd><span class="text--muted  text--light">&mdash;</span></dd>
                        </dl>

                        <dl>
                            <dt>Transmission</dt>
                            <dd>Automatic</dd>
                        </dl>

                        <dl>
                            <dt>Engine Fuel Type</dt>
                            <dd>Petrol</dd>
                        </dl>

                        <dl>
                            <dt>Enginee Capacity (cc)</dt>
                            <dd>2457 cc</dd>
                        </dl>

                        <dl>
                            <dt>Assemble Type</dt>
                            <dd>CKU</dd>
                        </dl>

                        <dl>
                            <dt>Nickname</dt>
                            <dd><span class="text--muted  text--light">&mdash;</span></dd>
                        </dl>

                        <dl>
                            <dt>V-Code</dt>
                            <dd>MY_SUBAR06001</dd>
                        </dl>
                    </div>
                </div>
            </div>
            <!--end: .panel -->
        </div>
        <!--end: .grid__item.one-third -->
    </div>
</div>

<?php include 'views/templates/foot.php'; ?>
