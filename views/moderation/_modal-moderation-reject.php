<div class="modal" tabindex="-1" role="dialog" id="moderation-reject">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-head">
                <!-- <div class="modal-close  icon  icon--close" data-dismiss="modal"></div> -->
                <h6 class="modal-title">Reject Reason</h6>
            </div>
            <div class="modal-body  hard">
                <div class="soft-md">
                    <?php include 'views/moderation/_select-reject-reasons.php'; ?>
                </div>

                <hr class="rule  rule--light">

                <div class="soft-md">
                    <div class="checkbox">
                        <input type="checkbox" id="send-sms" checked>
                        <span class="icon  icon--check"></span>
                        <label for="send-sms">Send SMS</label>
                    </div>

                    <textarea class="input  one-whole" disabled="" rows="2">Carlist.my: We regret to inform that your ad has been rejected. Click http://bit.ly/1UO37Xn to view reason.</textarea>
                </div>

                <hr class="rule  rule--light">

                <div class="soft-md">
                    <div class="checkbox">
                        <input type="checkbox" id="send-email" checked>
                        <span class="icon  icon--check"></span>
                        <label for="send-email">Send Email</label>
                    </div>

                    <div class="fill--shade" style="max-height:170px;overflow-y:auto">
                        <div class="soft-lg">
                            <p>Dear Valued Customer,</p>
                            <p>Thank you for posting your ad on Carlist.my, we strive to provide the best customer experience as we aim to be the most trusted automotive website in Malaysia.</p>
                            <p>We regret to inform you that your following advertisement;</p>
                            <p><strong>Ref ID: 4055927 was</strong><strong> not published</strong> due to non-compliance with our <a href="https://carlist.my/help#post-guidelines">quality guidelines</a>.</p>
                            <p><strong>Your listing was rejected due to the reason(s) stated below:</strong></p>
                        </div>
                        <div class="soft-lg--sides">
                            <textarea id="txt_reject_reason" name="txt_reject_reason" rows="4" class="input  one-whole" placeholder="Please select reject reason"></textarea>
                        </div>
                        <div class="soft-lg">
                            <p>Kindly review the details of your ad before resubmitting it.<br><br>You may refer to our <a href="https://carlist.my/help#post-guidelines">quality guidelines</a> for more information.</p>
                            <p>Do be informed that this ad will be automatically deleted from the system if you fail to edit it within 48 hours.</p>
                            <p>We apologize for any inconvenience caused.</p>
                            <p>Thank you and have a nice day.</p>
                            <p>Best Regards,</p>
                            <p><strong>Your </strong><strong>Carlist.my</strong><strong> Customer Care Team</strong><strong><br></strong>Suite 18.01, Level 18, Centrepoint North,<br>Mid Valley City, Lingkaran Syed Putra,<br>59200 Kuala Lumpur, Malaysia.</p>
                            <p>
                                <strong>O</strong> 1300 30 4227
                                <strong>E</strong> <a href="mailto:enquiries@carlist.my">enquiries@carlist.my</a>
                                <strong>F</strong> 03 2776 6010
                                <strong>W</strong> <span style="text-decoration: underline;"><a href="http://www.carlist.my/" target="_blank">www.carlist.my</a></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-foot">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn--negative">Confirm Reject</button>
            </div>
        </div>
    </div>
</div>
