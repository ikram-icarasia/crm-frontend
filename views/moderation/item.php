<div class="panel  relative">
    <div class="grid  grid--full  relative">
        <div class="grid__item  one-half  fill--shade  absolute  pin--left  pin--top  pin--bottom"></div>

        <div class="grid__item  one-half  relative">
            <div class="relative">
                <section class="soft-md">
                    <h6 class="head--five  text--bold  push-md--bottom  soft--bottom">Registration Card</h6>
                    <?php for($gallery=8; $gallery<9; $gallery++) { ?><img src="images/gallery/gallery-<?=$gallery?>.jpg" valign="top" class="block  push-sm--bottom"><?php } ?>
                </section>

                <hr class="rule  rule--light  flush">

                <section class="soft-md">
                    <h6 class="head--five  text--bold  push-md--bottom  soft--bottom">Ad Images</h6>
                    <?php for($gallery=1; $gallery<8; $gallery++) { ?><img src="images/gallery/gallery-<?=$gallery?>.jpg" valign="top" class="block  push-sm--bottom"><?php } ?>
                </section>
            </div>
        </div>

        <div class="grid__item  one-half">
            <section class="soft-md">
                <h6 class="head--five  text--bold  push-md--bottom  soft--bottom">Contact Details</h6>
                <div class="list-data  list-data--grid">
                    <dl>
                        <dt>Trusted Dealer</dt>
                        <dd>False</dd>
                    </dl>
                    <dl>
                        <dt>Company</dt>
                        <dd>Company</dd>
                    </dl>
                    <dl>
                        <dt>Person Name</dt>
                        <dd>Azri Abdullah</dd>
                    </dl>
                    <dl>
                        <dt>Email</dt>
                        <dd>azri.absullah@icarasia.com</dd>
                    </dl>
                    <dl>
                        <dt>Mobile Number</dt>
                        <dd>0169282863</dd>
                    </dl>
                    <dl>
                        <dt>Listing ID</dt>
                        <dd>4055927</dd>
                    </dl>
                    <dl>
                        <dt>Profile</dt>
                        <dd>Private</dd>
                    </dl>
                    <dl>
                        <dt>Username</dt>
                        <dd>azri.agogo@gmail.com</dd>
                    </dl>
                    <dl>
                        <dt>Package</dt>
                        <dd>Basic</dd>
                    </dl>
                </div>
            </section>

            <hr class="rule  rule--light  flush">

            <section class="soft-md">
                <h6 class="head--five  text--bold  push-md--bottom  soft--bottom">Ad Details</h5>
                <div class="list-data  list-data--grid">
                    <dl>
                        <dt>Vehicle Type</dt>
                        <dd>Used</dd>
                    </dl>
                    <dl>
                        <dt>Is Featured</dt>
                        <dd>False</dd>
                    </dl>
                    <dl>
                        <dt>Title</dt>
                        <dd>2000 Mercedes-Benz C200 2.0 Elegance Sedan</dd>
                    </dl>
                    <dl>
                        <dt>Price</dt>
                        <dd>
                            RM 28,000
                            -
                            <a href="#">Show Price Guide</a>

                            <div class="js-price-guide">
                                <table class="table  table--price-guide  text--center  push-sm--top">
                                    <thead>
                                        <tr>
                                            <th colspan="3" class="soft-md  text--negative  text--normal">
                                                -12% below from average price
                                            </th>
                                        </tr>

                                        <tr>
                                            <th colspan="3" class="soft-md  text--negative  text--normal">
                                                +14% above from average price
                                            </th>
                                        </tr>

                                        <tr>
                                            <th colspan="3" class="soft-md  text--positive  text--normal">
                                                -3% above from average price
                                            </th>
                                        </tr>

                                        <tr>
                                            <th colspan="3" class="soft-md  text--positive  text--normal">
                                                +8.5% above from average price
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Average</th>
                                            <th>Best Price</th>
                                            <th>Highest</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td>RM 100,076</td>
                                            <td>RM 122,800</td>
                                            <td>RM 133,800</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Body Type</dt>
                        <dd>Sedan</dd>
                    </dl>
                    <dl>
                        <dt>Make</dt>
                        <dd>Mercedes-Benz</dd>
                    </dl>
                    <dl>
                        <dt>Model</dt>
                        <dd>C200</dd>
                    </dl>
                    <dl>
                        <dt>Year</dt>
                        <dd>2000</dd>
                    </dl>
                    <dl>
                        <dt>Registration Year</dt>
                        <dd>2017</dd>
                    </dl>
                    <dl>
                        <dt>Variant</dt>
                        <dd>Elegance</dd>
                    </dl>
                    <dl>
                        <dt>Nick Name</dt>
                        <dd>
                            <div class="text--negative">
                                Not Stated
                            </div>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Transmission</dt>
                        <dd>Automatic</dd>
                    </dl>
                    <dl>
                        <dt>Fuel Type</dt>
                        <dd>Petrol - Unleaded (ULP)</dd>
                    </dl>
                    <dl>
                        <dt>Enginee Capacity (cc)</dt>
                        <dd>1998</dd>
                    </dl>
                    <dl>
                        <dt>Color</dt>
                        <dd>Blue</dd>
                    </dl>
                    <dl>
                        <dt>Mileage (km)</dt>
                        <dd>295 - 300K</dd>
                    </dl>
                    <dl>
                        <dt>Reference Number</dt>
                        <dd>
                            <div class="text--negative">
                                Not Stated
                            </div>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Location</dt>
                        <dd>Selangor</dd>
                    </dl>
                    <dl>
                        <dt>Published Date</dt>
                        <dd>
                            <div class="text--negative">
                                Not Stated
                            </div>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Updated Date</dt>
                        <dd>18/09/2017 11:26:32 AM</dd>
                    </dl>
                    <dl>
                        <dt>Status</dt>
                        <dd>Pending</dd>
                    </dl>
                    <dl>
                        <dt>Moderator</dt>
                        <dd>Nor Azri</dd>
                    </dl>
                    <dl>
                        <dt>Platform</dt>
                        <dd>SP Desktop</dd>
                    </dl>
                    <dl>
                        <dt>Description</dt>
                        <dd>
                            <p>https://www.google.com/search?q=lukas+podolski&rlz=1C5CHFA_enMY753MY755&oq=lukas+po&aqs=chrome.0.0j69i57j0l4.9530j0j1&sourceid=chrome&ie=UTF-8</p>
                            Kereta dijual untuk beli kereta baru. Engine mantap. Gearbox baru service. Air-cond sejuk. Warna biru pada kereta masih cantik dan berkilat. Shock Absorber 4 biji baru ditukar. Bumper depan telah ditukar kepada Sport Bumper. Spoiler pun telah ditambah. Grille depan telah ditukar kepada Emblem Mercedes-Benz yang besar. Headlamp pun telah ditukar kepada Sport Version. Emblem Mercedes-Benz pada Bonnet telah ditukar kepada jenis yang rata. Steering Wheel telah ditukar kepada Leather Wrapped dengan Walnut. Tayar lebih kurang 70%. Exhaust pakai Twin Tailpipe. Harga kereta tak termasuk nombor 51. Nombor 51 yang sedia ada akan dipindahkan ke kereta baru. Harga pada iklan adalah harga terbaik.
                        </dd>
                    </dl>
                </div>
            </section>

            <br>
            <br>
            <br>
            <br>

            <section class="soft-md  absolute  pin--bottom  pin--right  one-half">
                <div class="float--right">
                    <a href="moderation-edit.php" class="btn">Edit Ad</a>
                    <a href="#" class="btn  btn--positive">Approve</a>
                </div>
                <a href="#" class="btn  btn--negative" data-toggle="modal" data-target="#moderation-reject">Reject</a>
            </section>

            <section class="soft-md  absolute  pin--bottom  pin--right  one-half  ---------  visuallyhidden">
                <div class="alert  alert--error  pack">
                    <div class="pack__item  tight  soft-md--right">
                        <div class="alert__label">Error</div>
                    </div>
                    <div class="pack__item">
                        SFL user has insufficient balance, listing sent to draft.
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
