<div class="selectize--disable-input  selectize--dropdown-right  selectize--dropdown-full">
    <select id="ddlRejectReason" name="ddlRejectReason" class="select  js-selectize">
        <option value="none">-- Select Reject Reason --</option>
        <option value="Model/Make not launched in Malaysia The vehicle listed has yet to be launched in the Malaysian market. We would highly recommended to wait for the official launch from the manufacturer to confirm the actual specs and price to eliminate possibility of inaccurate information.">Car Make/Model yet to be Released Officially</option>
        <option value="New Car/Recon Car advertisement. Unfortunately, Direct Owners are limited to advertising Used Cars only. Do contact our customer care personnel to register yourself as a Sales Agent or Company account if you are not a direct owner.">Direct Owner posting New Car/Recond Car</option>
        <option value="Duplicate advertisement. Kindly be informed that all users are allowed to list only one advertisement per make, per model, per variant. This is to avoid multiple advertisements and confusion to buyers.">Duplication of Advertisement</option>
        <option value="Misleading terms used Your advertisement contains statements such as &quot;maximum loan&quot;, &quot;100% loan&quot;, &quot;full loan&quot;, &quot;zero downpayment&quot;, &quot;zero booking fee&quot; and other similar terms without a proper disclaimer. We have received numerous feedback on this term being confusing and misleading. Kindly edit your advertisement by adding words like &quot;subject to bank approval&quot; in your advertisement to avoid confusion.">Full Loan/Zero Downpayment Statement</option>
        <option value="Advertisement contains inconsistent make/model/variant information in the title and/or description field. As per the subject above, there are inconsistencies between your title and description. Please be informed that each listing is allowed to advertise only one model/make/variant. You would need to create another advertisement if you are selling another car with a different model/make/variant. This way you can better target customers to your advertisements.">Inconsistent Make &amp; Model Advertised</option>
        <option value="Inconsistent information.

        Information with regards to the transmission/mileage/location of the car stated in the advertisement title, and the transmission/mileage/location stated in the description are inconsistent. Please ensure the information provided is accurate to avoid confusion for potential buyers.">Inconsistent Transmission/Mileage/Location</option>
        <option value="Pricing is incorrect/inconsistent.

        As per the subject above, there is incorrect/inconsistent pricing listed within the title, pricing field or description field. Please ensure the information provided is accurate to minimise confusion for potential buyers.">Incorrect/Inconsistent Pricing</option>
        <option value="Incorrect Year of Manufacture.

        As per the information provided, you may have accidentally keyed in year of registration instead of the manufactured year of the vehicle.

        We provide the year of manufacture to minimise confusion for potential buyers.">Incorrect Year</option>
        <option value="Misleading information

        The advertisement contains loan amount, downpayment amount, monthly payment amount or interest rate without a proper disclaimer.

        We have received numerous feedback on this term being confusing and misleading.

        Kindly edit your advertisement by adding words like &quot;subject to bank approval&quot; in your advertisement to avoid confusion.">Loan/Downpayment/Interest Rate Amount</option>
        <option value="Multiple Make/ Model/ Variant/ Spec per advertisement

        The advertisement contains multiple make/model/variant/specifications and images.

        Kindly be informed that only one Model/Make/Variant/Specification is allowed per advertisement with the correct manufacturing year, price and images.

        Doing so can maximise the exposure of your advertisement and hence bring you more potential buyers.

        Please also include the actual images of the car as the majority of online buyers are more interested in viewing the images of the car being sold instead of the generic images provided by the official manufacturer.">Multiple Make &amp; Model per Advertisement</option>
        <option value="Non OTR pricing listed

        Price provided in the advertisement is a non-OTR price.

        We provide price of car WITHOUT insurance because preference of insurance coverage varies from different individuals.

        Should the OTR price provided in our site be incorrect, please report it to us and we will rectify it as soon as possible.">New Car Pricing Not OTR Price</option>
        <option value="Other Services Offered

        Carlist.my is an online automobile portal catered to providing a platform for the buying/selling of vehicles. You can add service like renewal of license or road tax.

        However, any other unrelated services offered will be seen as a violation and will be removed from our site.">Other Services Offered</option>
        <option value="Sambung Bayar advertisement

        Effective 14th January 2013, Carlist.my no longer offers Sambung Bayar as part of our services. This is to ensure minimal opportunities for fraud within our site to improve the level of confidence of our site visitors.

        As Carlist.my strives to provide the best buying experience for all our customers, we appreciate your kind understanding with regards to this matter.">Sambung Bayar Advertisement</option>
        <option value="[Advertisement contains non-Carlist watermark or  other URL or image contain phone number]

        All information and images posted in the advertisements must not contain watermarks of other organisations or other URLs as these watermarks/URLs are the copyright of the aforementioned entities.

        In order to avoid such events, please amend the information and images posted and resubmit the amended advertisement(s).">Watermark/URL of Another Website</option>
        <option value="Advertisement is listed in the wrong category.

        In carlist.my, we have catergorised all cars for sale into New Cars, Used Cars and Accessories. This is to ease the search process on the site. As per the information provided, we find that the advertisement has been placed in the wrong category.

        Kindly resubmit a new advertisement with the correct category selected.">Wrong Listing Category</option>
        <option value="[Advertisement with less than 3 images or small images or portrait images or blurry or without exterior photo]

        Advertisement with less than 3 images or small images or portrait images or blurry or without exterior photo.

        Kindly upload clear images of the car. Doing so can enhance the advertisement's quality and maximize the exposure of your advertisement and hence bring you more potential buyers.

        Remember, ads with 8+ photos get more interest than cars with just 1 or 2. So add more pictures as well.">Blurry or No Image</option>
        <option value="Advertisement with invalid contact number/contact number in the title field.

        The advertisement title contains a contact number or the Carlist.my profile contains an invalid contact number.

        Do not worry, your profile and contact number is prominently displayed next to your advertisement. You may use the extra space to further explain the selling points of your car.">Invalid Contact Number/Contact Number in the Ad Title</option>
    </select>
</div>
