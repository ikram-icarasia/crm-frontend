<nav class="pagination  pack">
    <div class="pagination__prev  pack__item  one-sixth  palm-one-half  soft-quarter--right">
        <a href="#" class="btn  btn--full  btn--disabled">Prev</a>
    </div>
    <div class="pagination__numbers  pack__item  text--center  visuallyhidden--palm">
        <a href="#" class="active">1</a>
        <a href="#">2</a>
        <a href="#">3</a>
        <a href="#">4</a>
        <a href="#">5</a>
        <a href="#">6</a>
        <a href="#">7</a>
        <a href="#">8</a>
        <a href="#">9</a>
        <a href="#">10</a>
    </div>
    <div class="pagination__next  pack__item  one-sixth  palm-one-half  soft-quarter--left">
        <a href="#" class="btn  btn--full">Next</a>
    </div>
</nav>
