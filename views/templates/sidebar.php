<?php
    function seoURL($string) {
        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Clean up &
        $string = str_replace("&", "", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        return $string;
    }

    // Main Menu
    $menu_main = array(
        "1"  => array("label"=>"Home", "icon"=>"icon--home", "url"=>"index", "sub" => 0 ),
        "5"  => array("label"=>"Bulk Removal", "icon"=>"icon--delete-shield",
                            "sub"=>array(
                                "1"  => array("label"=>"Remove Listings", "url"=>"404", ),
                                "2"  => array("label"=>"Remove Progress", "url"=>"404", ),
                            ) ),
        "4"  => array("label"=>"Bulk Upload", "icon"=>"icon--upload",
                            "sub"=>array(
                                "1"  => array("label"=>"Upload Listings", "url"=>"404", ),
                                "2"  => array("label"=>"Upload Progress", "url"=>"404", ),
                            ) ),
        "3"  => array("label"=>"Customer", "icon"=>"icon--users",
                            "sub"=>array(
                                "1"  => array("label"=>"Acount", "url"=>"404", ),
                                "2"  => array("label"=>"Badge", "url"=>"404", ),
                                "3"  => array("label"=>"Contact", "url"=>"404", ),
                                "4"  => array("label"=>"Email Alert", "url"=>"404", ),
                                "5"  => array("label"=>"Email History", "url"=>"404", ),
                                "6"  => array("label"=>"Profile", "url"=>"404", ),
                                "7"  => array("label"=>"Lead", "url"=>"404", ),
                                "8"  => array("label"=>"Saved Cars", "url"=>"404", ),
                                "9"  => array("label"=>"Login Audit", "url"=>"404", ),
                                "10" => array("label"=>"Transaction History (Credit)", "url"=>"404", ),
                                "11" => array("label"=>"Transaction History (Payment)", "url"=>"404", ),
                                "12" => array("label"=>"QuickBlog Chats", "url"=>"404", ),
                                "13" => array("label"=>"Subscription Product", "url"=>"404", ),
                                "14" => array("label"=>"Listings", "url"=>"404", ),
                            ) ),
        "8"  => array("label"=>"Customer Care", "icon"=>"icon--support",
                            "sub"=>array(
                                "1"  => array("label"=>"Activities", "url"=>"404", ),
                                "2"  => array("label"=>"Cases", "url"=>"404", ),
                                "3"  => array("label"=>"Views", "url"=>"404", ),
                            ) ),
        "7"  => array("label"=>"Finance", "icon"=>"icon--piggy-bank",
                            "sub"=>array(
                                "1"  => array("label"=>"Invoice", "url"=>"404", ),
                                "2"  => array("label"=>"Offline Transaction", "url"=>"404", ),
                                "3"  => array("label"=>"Subscription Product", "url"=>"404", ),
                                "4"  => array("label"=>"Product Catalogue", "url"=>"404", ),
                            ) ),
        "21"  => array("label"=>"Listings", "icon"=>"icon--card", "url"=>"listings",  "sub" => 0 ),
        "6"  => array("label"=>"Moderation", "icon"=>"icon--task",
                            "sub"=>array(
                                "1"  => array("label"=>"Dashboard", "url"=>"moderation-dashboard", ),
                                "2"  => array("label"=>"Listings", "url"=>"moderation-listing", ),
                                "3"  => array("label"=>"Listings Pre Moderation", "url"=>"404", ),
                                "4"  => array("label"=>"Listings Post Moderation", "url"=>"404", ),
                                "5"  => array("label"=>"Video Pre Moderation", "url"=>"moderation-video", ),
                                "6"  => array("label"=>"Video Post Moderation", "url"=>"moderation-video", ),
                                "7"  => array("label"=>"Vehicle Approval", "url"=>"404", ),
                            ) ),
        "2"  => array("label"=>"Operations", "icon"=>"icon--rocket",
                            "sub"=>array(
                                "1"  => array("label"=>"Assign Credits", "url"=>"404", ),
                                "2"  => array("label"=>"Offline Transaction", "url"=>"404", ),
                                "3"  => array("label"=>"Premium Enquiry", "url"=>"404", ),
                                "4"  => array("label"=>"Subscription Product", "url"=>"404", ),
                            ) ),
        "9"  => array("label"=>"Reports", "icon"=>"icon--report",
                            "sub"=>array(
                                "0"  => array("label"=>"AMBDM Transactions", "url"=>"404", ),
                                "1"  => array("label"=>"Credit Transaction History", "url"=>"404", ),
                                "2"  => array("label"=>"Payment Transaction History", "url"=>"404", ),
                                "3"  => array("label"=>"Transaction Progress", "url"=>"404", ),
                            ) ),
        "12" => array("label"=>"Response Pro", "icon"=>"icon--response",
                            "sub"=>array(
                                "1"  => array("label"=>"Forwarding Call", "url"=>"404", ),
                                "2"  => array("label"=>"Forwarding SMS", "url"=>"404", ),
                                "3"  => array("label"=>"Forwarding Transaction", "url"=>"404", ),
                                "4"  => array("label"=>"Online Lead", "url"=>"404", ),
                            ) ),
        "10" => array("label"=>"Settings", "icon"=>"icon--settings",
                            "sub"=>array(
                                "1"  => array("label"=>"Badge Templates", "url"=>"404", ),
                                "2"  => array("label"=>"Email Templates", "url"=>"404", ),
                                "3"  => array("label"=>"SMS Templates", "url"=>"404", ),
                            ) ),
        "11" => array("label"=>"User Management", "icon"=>"icon--sign-user",
                            "sub"=>array(
                                "1"  => array("label"=>"Account Representative", "url"=>"404", ),
                                "2"  => array("label"=>"Claims", "url"=>"404", ),
                                "3"  => array("label"=>"Roles", "url"=>"404", ),
                                "4"  => array("label"=>"User", "url"=>"404", ),
                            ) ),

    );
?>
<nav class="sidebar  fixed  full--height  transition--default">
    <section class="sidebar__head  relative  tier--two">
        <div class="sidebar__title  text--semibold  transition--default  relative  valign--top  no--wrap">
            iCarAsia CRM

            <span class="sidebar__avatar  absolute  square  square--32  valign--middle  pin--top  pin--right  circle  visuallyhidden"></span>
        </div>
    </section>
    <section class="sidebar__body  full--width  absolute  full--height  tier--one">
        <nav class="sidebar__scroll  no--overflow-x  overflow-y  absolute  full--width">
            <ul class="sidebar__menu  list--reset">
                <?php foreach ($menu_main as $item => $menu) { ?>
                    <?php if(!$menu['sub']) {?>
                        <li>
                            <a href="<?=$menu['url']?>.php" class="pack">
                                <span class="menu__icon  pack__item  tight">
                                    <span class="icon  <?=$menu['icon']?>"></span>
                                </span>
                                <span class="menu__name  pack__item">
                                    <?=$menu['label']?>
                                </span>
                            </a>
                        </li>
                    <?php } else { ?>
                        <li class="collapsible">
                            <a class="collapsible__toggle  pack  collapsed" data-toggle="collapse" href="#collapse--<?=seoURL($menu['label'])?>">
                                <span class="menu__icon  pack__item  tight">
                                    <span class="icon  <?=$menu['icon']?>"></span>
                                </span>
                                <span class="menu__name  pack__item">
                                    <?=$menu['label']?>
                                </span>
                                <span class="menu__toggle  pack__item  tight">
                                    <i class="collapsible__icon  icon  icon--chevron-down  transition--default  block"></i>
                                </span>
                            </a>
                            <ul id="collapse--<?=seoURL($menu['label'])?>" class="collapsible__menu  collapse  list--reset">
                                <?php foreach ($menu['sub'] as $item => $menu) { ?>
                                    <li><a href="<?=$menu['url']?>.php"><?=$menu['label']?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php }?>
                <?php } ?>
            </ul>

            <h6 class="visuallyhidden     sidebar__subtitle  text--uppercase  text--normal  transition--default">Elements</h6>
            <ul class="visuallyhidden     sidebar__menu  list--reset">
                <li>
                    <a href="documentation.php" class="pack">
                        <span class="menu__icon  pack__item  tight">
                            <span class="square  square--20  valign--middle  float--left"></span>
                        </span>
                        <span class="menu__name  pack__item">
                            Documentation
                        </span>
                    </a>
                </li>

                <li>
                    <a href="#" class="pack">
                        <span class="menu__icon  pack__item  tight">
                            <span class="square  square--20  valign--middle  float--left"></span>
                        </span>
                        <span class="menu__name  pack__item">
                            Colors
                        </span>
                    </a>
                </li>

                <li>
                    <a href="#" class="pack">
                        <span class="menu__icon  pack__item  tight">
                            <span class="square  square--20  valign--middle  float--left"></span>
                        </span>
                        <span class="menu__name  pack__item">
                            Icons
                        </span>
                    </a>
                </li>

                <li>
                    <a href="documentation--typography.php" class="pack">
                        <span class="menu__icon  pack__item  tight">
                            <span class="square  square--20  valign--middle  float--left"></span>
                        </span>
                        <span class="menu__name  pack__item">
                            Typography
                        </span>
                    </a>
                </li>

                <li>
                    <a href="documentation--layouts.php" class="pack">
                        <span class="menu__icon  pack__item  tight">
                            <span class="square  square--20  valign--middle  float--left"></span>
                        </span>
                        <span class="menu__name  pack__item">
                            Layouts
                        </span>
                    </a>
                </li>

                <li>
                    <a href="documentation--forms.php" class="pack">
                        <span class="menu__icon  pack__item  tight">
                            <span class="square  square--20  valign--middle  float--left"></span>
                        </span>
                        <span class="menu__name  pack__item">
                            Forms
                        </span>
                    </a>
                </li>

                <li>
                    <a href="documentation--buttons.php" class="pack">
                        <span class="menu__icon  pack__item  tight">
                            <span class="square  square--20  valign--middle  float--left"></span>
                        </span>
                        <span class="menu__name  pack__item">
                            Buttons
                        </span>
                    </a>
                </li>
            </ul>

            <h6 class="visuallyhidden     sidebar__subtitle  text--uppercase  text--normal  transition--default">Javascript</h6>
            <ul class="visuallyhidden     sidebar__menu  list--reset">
                <li>
                    <a href="documentation--javascript.php" class="pack">
                        <span class="menu__icon  pack__item  tight">
                            <span class="square  square--20  valign--middle  float--left"></span>
                        </span>
                        <span class="menu__name  pack__item">
                            Dropdown
                        </span>
                    </a>
                </li>

                <li>
                    <a href="documentation--javascript.php" class="pack">
                        <span class="menu__icon  pack__item  tight">
                            <span class="square  square--20  valign--middle  float--left"></span>
                        </span>
                        <span class="menu__name  pack__item">
                            Tabs
                        </span>
                    </a>
                </li>

                <li>
                    <a href="documentation--javascript.php" class="pack">
                        <span class="menu__icon  pack__item  tight">
                            <span class="square  square--20  valign--middle  float--left"></span>
                        </span>
                        <span class="menu__name  pack__item">
                            Modal
                        </span>
                    </a>
                </li>

                <li>
                    <a href="documentation--javascript.php" class="pack">
                        <span class="menu__icon  pack__item  tight">
                            <span class="square  square--20  valign--middle  float--left"></span>
                        </span>
                        <span class="menu__name  pack__item">
                            Collapse
                        </span>
                    </a>
                </li>

                <li>
                    <a href="documentation--javascript.php" class="pack">
                        <span class="menu__icon  pack__item  tight">
                            <span class="square  square--20  valign--middle  float--left"></span>
                        </span>
                        <span class="menu__name  pack__item">
                            Selectize
                        </span>
                    </a>
                </li>
            </ul>
        </nav>
    </section>
    <section class="sidebar__foot"></section>
</nav>

<nav class="sidebar-backdrop  js-toggle-sidebar  fixed  full--width  full--height  visuallyhidden"></nav>
