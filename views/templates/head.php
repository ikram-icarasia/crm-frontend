<?php include 'setup.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <link href="https://fonts.googleapis.com/css?family=Heebo:400,700|Roboto:400,700" rel="stylesheet">
    <link type="text/css" href="styles/style.css" rel="stylesheet">
    <!-- <link type="text/css" href="docs/scripts/prism.css" rel="stylesheet"> -->
</head>
<body class="<?=$body_class?>">
    <?php if($page_sidebar != 'false') { ?>
    <?php include 'views/templates/sidebar.php'; ?>
    <?php } ?>

    <?php if($page_toolbar != 'false') { ?>
        <nav class="toolbar  toolbar--main">
            <div class="toolbar__menu  pack">
                <?php if($page_sidebar != 'false') { ?>
                <div class="pack__item  tight  soft-lg--right">
                    <a href="javascript:void(0)" class="toggle-sidebar  js-toggle-sidebar">
                        <span class="bar  bar-1"></span>
                        <span class="bar  bar-2"></span>
                        <span class="bar  bar-3"></span>
                    </a>
                </div>
                <?php } ?>

                <div class="pack__item">
                    <h5 class="text--normal  flush" style="line-height: 1;">
                        <?php $path__count = 0; ?>
                        <?php if($breadcrumbs) { ?>
                            <?php foreach ($breadcrumbs as $breadcrumbs__path) { ?>
                                <?php if($path__count > 0) {?><span class="text--muted  text--light  visuallyhidden--palm  push-xs--sides">&rsaquo;</span><?php } ?>
                                <a href="#"><?php echo $breadcrumbs__path; ?></a>
                                <?php $path__count ++; ?>
                            <?php } ?>
                        <?php } else { ?>
                        No Breadcrumb Defined
                        <?php } ?>
                    </h5>
                </div>

                <div class="pack__item  tight">
                    <div class="dropdown  dropdown--right">
                        <a class="dropdown__control" data-toggle="dropdown" href="javascript:void(0)">
                            <div class="pack">
                                <div class="pack__item  tight">
                                    <img src="images/avatars/3.png" width="32" height="32" class="circle  block" />
                                </div>

                                <div class="pack__item  soft-sm--left  text--white">
                                    Parveen Kumar
                                </div>
                            </div>
                        </a>
                        <div class="dropdown__menu">
                            <ul class="dropdown__list">
                                <li><a href="#">Edit Profile</a></li>
                                <li><a href="#">Change Password</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>


        <?php if($toolbar_sub) { ?>
        <nav class="toolbar  toolbar--sub">
            <?php include $toolbar_sub; ?>
        </nav>
        <?php } // endif $toolbar_sub ?>
    <?php } // endif $page_toolbar ?>

    <article class="page  clearfix">
