<?php
	$project_title = "Carlist Design Assets";
	$project_title_long = $project_title;

	// Form: Label
	function html_label($label = null, $id = null) {
        if ($label != null) {
            if ($id != null) {
                $html  .= "<label for=" . $id . ">" . $label . "</label>";
            } else {
                $html  .= "<label>" . $label . "</label>";
            }
        }

        return $html;
    }

	// Form: Input
    function html_input($label = null, $placeholder = null, $id = null, $class = 'text-input') {
        $html .= '<div class="form-group">';
            $html .= html_label($label, $id);

            $html .= "<div>";
    		    $html .= "<input type=\"{$type}\" class=\"{$class}\"";
                    if ($placeholder != null) {
                        $html  .= " placeholder=\"{$placeholder}\"";
                    }

                    if ($id != null) {
                        $html  .= " name=\"{$id}\"";
                        $html  .= " id=\"{$id}\"";
                    }
                $html .= ">";
            $html .= "</div>";
		$html .= "</div>";

		echo $html;
	}
