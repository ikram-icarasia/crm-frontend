</article>
<script src="scripts/jquery-3.2.1.min.js"></script>
<script src="docs/scripts/prism.js"></script>

<!-- <script type="text/javascript" src="scripts/bootstrap/transition.js"></script> -->
<script type="text/javascript" src="scripts/bootstrap/collapse.js"></script>
<script type="text/javascript" src="scripts/bootstrap/dropdown.js"></script>
<script type="text/javascript" src="scripts/bootstrap/tab.js"></script>
<script type="text/javascript" src="scripts/bootstrap/modal.js"></script>
<script type="text/javascript" src="scripts/vendor/selectize.min.js"></script>
<script type="text/javascript" src="scripts/vendor/pickadate/compressed/picker.js"></script>
<script type="text/javascript" src="scripts/vendor/pickadate/compressed/picker.date.js"></script>
<script type="text/javascript" src="scripts/vendor/pickadate/compressed/picker.time.js"></script>
<script type="text/javascript" src="scripts/default.js"></script>

</body>
</html>
