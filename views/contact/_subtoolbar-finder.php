<form class="toolbar__menu  pack  no--wrap">
    <div class="pack__item  soft-sm--right">
        <input type="text" class="input  one-whole" placeholder="Search by User Name">
    </div>

    <div class="pack__item  soft-sm--right">
        <input type="text" class="input  one-whole" placeholder="Search by Email">
    </div>

    <div class="pack__item  soft-sm--right">
        <input type="text" class="input  one-whole" placeholder="Search by Phone Number">
    </div>

    <div class="pack__item">
        <input type="button" class="btn  btn--full  btn--primary" value="Search">
    </div>
</form>
