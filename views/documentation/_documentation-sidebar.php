<ul class="documentation-sidebar  nav  nav--stacked">
    <li>
        <a href="index.php" class="icon  icon--school  text--semibold">Documentation</a>
    </li>

    <li>
        <a href="#" class="icon  icon--palette  text--semibold">Colors</a>
    </li>

    <li>
        <a href="typography.php" class="icon  icon--text  text--semibold">Typography</a>

        <ul class="nav  nav--stacked">
            <li>
                <a class="muted" href="#">Typeface</a>
            </li>

            <li>
                <a class="muted" href="#">Headings</a>
            </li>

            <li>
                <a class="muted" href="#">Links</a>
            </li>

            <li>
                <a class="muted" href="#">Lists</a>
            </li>

            <li>
                <a class="muted" href="#">Utilities</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="#" class="icon  icon--star  text--semibold">Icons</a>
    </li>

    <li>
        <a href="layouts.php" class="icon  icon--web  text--semibold">Layouts</a>

        <ul class="nav  nav--stacked">
            <li class="muted">
                <a href="#">Container</a>
            </li>

            <li>
                <a href="#">Widths</a>
            </li>

            <li>
                <a href="#">Floats</a>
            </li>

            <li>
                <a href="#">Grids</a>
            </li>

            <li>
                <a href="#">pack</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="objects.php" class="icon  icon--whatshot  text--semibold">Objects</a>

        <ul class="nav  nav--stacked">
            <li>
                <a href="objects--buttons.php">Buttons</a>

                <ul class="nav  nav--stacked  visuallyhidden">
                    <li>
                        <a href="#">Usage</a>
                    </li>

                    <li>
                        <a href="#">Sizes</a>
                    </li>

                    <li>
                        <a href="#">Options</a>
                    </li>

                    <li>
                        <a href="#">Disabled</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="objects--form.php">Forms</a>

                <ul class="nav  nav--stacked  visuallyhidden">
                    <li class="muted">
                        <a class="muted" href="#">Labels</a>
                    </li>

                    <li>
                        <a href="#">Checkboxes</a>
                    </li>

                    <li>
                        <a href="#">Radios</a>
                    </li>

                    <li>
                        <a href="#">Text inputs</a>
                    </li>

                    <li class="muted">
                        <a href="#" class="muted">Text input groups</a>
                    </li>

                    <li>
                        <a href="#">Text areas</a>
                    </li>

                    <li class="muted">
                        <a href="#" class="muted">Control groups</a>
                    </li>

                    <li>
                        <a href="#">Selects</a>
                    </li>

                    <li class="muted">
                        <a href="#" class="muted">File upload</a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>

    <li>
        <a href="components.php" class="icon  icon--calendar  text--semibold">Components</a>

        <ul class="nav  nav--stacked">
            <li>
                <a href="components.php#dropdown">Dropdown</a>
            </li>
            <li>
                <a href="components.php#tab">Tabs</a>
            </li>
            <li>
                <a href="components.php#modal">Modal</a>
            </li>
        </ul>
    </li>
</ul>
