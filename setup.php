<?php
// Config
$body_class         = isset($body_class)  && $body_class  ? $body_class  : '';
$page_toolbar       = isset($page_toolbar) && $page_toolbar ? $page_toolbar : 'true';
$page_sidebar       = isset($page_sidebar) && $page_sidebar ? $page_sidebar : 'true';
$toolbar_sub        = isset($toolbar_sub)  && $toolbar_sub  ? $toolbar_sub  : '';
$toolbar__button    = isset($toolbar__button)  && $toolbar__button  ? $toolbar__button  : '';
