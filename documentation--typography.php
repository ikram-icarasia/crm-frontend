<?php include 'views/templates/head.php'; ?>

    <h2 class="push-sm--bottom">Typography</h2>

    <p style="font-size: 120%">Headings, paragraphs, blockquotes, lists, and more have some global resets.</p>

    <hr>

    <?php include 'docs/typography--text__typeface.php'; ?>

    <hr>

    <?php include 'docs/typography--text__headings.php'; ?>

    <hr>

    <?php include 'docs/typography--text__size.php'; ?>

    <hr>

    <?php include 'docs/typography--text__weight.php'; ?>

    <hr>

    <?php include 'docs/typography--text__alignment.php'; ?>

    <hr>

    <?php include 'docs/typography--text__cases.php'; ?>

    <hr>

    <?php include 'docs/typography--text__truncate.php'; ?>

<?php include 'views/templates/foot.php'; ?>


<script type="text/javascript">
    // Pass variables for typography
    $(document).ready(function(){
        var font_family = $('body').css('font-family').split(",");
        $(".body-fontfamily").append(font_family[0]);

        var font_size = $('body').css('font-size');
        $(".body-fontsize").append(parseInt(font_size));

        var h1_size = $(".documentation-example h1").css('font-size');
        $(".h1-fontsize").append(parseInt(h1_size));

        var h2_size = $(".documentation-example h2").css('font-size');
        $(".h2-fontsize").append(parseInt(h2_size));

        var h3_size = $(".documentation-example h3").css('font-size');
        $(".h3-fontsize").append(parseInt(h3_size));

        var h4_size = $(".documentation-example h4").css('font-size');
        $(".h4-fontsize").append(parseInt(h4_size));

        var h5_size = $(".documentation-example h5").css('font-size');
        $(".h5-fontsize").append(parseInt(h5_size));

        var h6_size = $(".documentation-example h6").css('font-size');
        $(".h6-fontsize").append(parseInt(h6_size));

        var alpha_size = $('.alpha').css('font-size');
        $(".alpha-fontsize").append(parseInt(alpha_size));

        var beta_size = $('.beta').css('font-size');
        $(".beta-fontsize").append(parseInt(beta_size));

        var gamma_size = $('.gamma').css('font-size');
        $(".gamma-fontsize").append(parseInt(gamma_size));

        var delta_size = $('.delta').css('font-size');
        $(".delta-fontsize").append(parseInt(delta_size));

        var epsilon_size = $('.epsilon').css('font-size');
        $(".epsilon-fontsize").append(parseInt(epsilon_size));

        var zeta_size = $('.zeta').css('font-size');
        $(".zeta-fontsize").append(parseInt(zeta_size));

        var milli_size = $('.milli').css('font-size');
        $(".milli-fontsize").append(parseInt(milli_size));

        var micro_size = $('.micro').css('font-size');
        $(".micro-fontsize").append(parseInt(micro_size));
    });
</script>
