
<?php
// Include subtoolbar
$toolbar_sub = 'views/listings/_subtoolbar-finder.php';

// Setup breadcrumbs
$breadcrumbs = array('Listings');

include 'views/templates/head.php';
?>

<main>
    <div class="panel">
        <table class="panel__table  table">
            <thead>
                <tr>
                    <th class="tight">
                        <label class="checkbox  flush">
                            <input type="checkbox">
                            <span class="icon  icon--check"></span>
                        </label>
                    </th>
                    <th valign="bottom" class="one-third">Title</th>
                    <th valign="bottom" class="one-sixth">Seller</th>
                    <th valign="bottom" class="one-twelfth">Status</th>
                    <th valign="bottom" class="one-twelfth">Created</th>
                    <th valign="bottom" class="one-twelfth">Published</th>
                    <th valign="bottom" class="one-twelfth">Modified</th>
                    <th valign="bottom" class="no--wrap  one-sixth">Assigned to</th>
                    <th valign="bottom" class="tight">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php for($count=1; $count<11; $count++) { ?>
                    <tr>
                        <td class="tight" valign="top">
                            <label class="checkbox  flush">
                                <input type="checkbox">
                                <span class="icon  icon--check"></span>
                            </label>
                        </td>
                        <td valign="top">
                            <a href="#">2008 Toyota Vios 1.5 E FULL Spec (AUTO)2008 Only 1 Careful LADY Owner, 83K Mileage, TIPTOP, ACCIDENT-Free, DIRECT-Owner, NEGOTIABLE wit AIRBEG+DVD+GPS</a>
                            <div class="push-sm--top">
                                <span class="push-sm--right">
                                    <span class="text--muted">Listing ID:</span>
                                    4153055
                                </span>
                            </div>
                        </td>
                        <td valign="top" data-tooltip="Seller & Profile" data-tooltip-position="top-left">
                            <a href="#">motorwaytrading</a>
                            <hr class="rule--light  push-sm--ends  six-tenths">
                            <div class="micro  text--uppercase  text--muted  push-xs--bottom">Profile</div>
                            <a href="#">(Sales Agent) Kcars Auto Sdn Bhd</a>
                        </td>
                        <td valign="top" class="no--wrap" data-tooltip="Status" data-tooltip-position="top-left">
                            <span class="text--positive">Submitted</span>
                        </td>
                        <td valign="top" class="no--wrap" data-tooltip="Created" data-tooltip-position="top-left">
                            29 Sep 2017
                            <br>
                            <span class="milli  text--muted">1:27:17 AM</span>
                        </td>

                        <td valign="top" class="no--wrap" data-tooltip="Published" data-tooltip-position="top-left">
                            <span class="text--negative">Not Published</span>
                        </td>

                        <td valign="top" class="no--wrap" data-tooltip="Modified" data-tooltip-position="top-left">
                            5 Oct 2017
                            <br>
                            <span class="milli  text--muted">3:03:46 AM</span>
                        </td>

                        <td valign="top" data-tooltip="Assigned to & Moderator" data-tooltip-position="top-left">
                            <a href="#">Karthik Radhakrishnan</a>
                            <hr class="rule--light  push-sm--ends  six-tenths">
                            <div class="micro  text--uppercase  text--muted  push-xs--bottom">Moderator</div>
                            <a href="#">Nasrullah Haikal</a>
                        </td>
                        <td valign="top" class="tight" width="1%">
                            <a href="#" class="btn  btn--icon" data-tooltip="Listing Details">
                                <i class="icon  icon--info"></i>
                            </a>

                            <a href="#" class="btn  btn--icon" data-tooltip="Edit Listing">
                                <i class="icon  icon--edit"></i>
                            </a>

                            <a href="#" class="btn  btn--icon" data-tooltip="Assign Listing">
                                <i class="icon  icon--tag"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

    <?php include 'views/templates/pagination.php'; ?>
</main>

<?php include 'views/templates/foot.php'; ?>
