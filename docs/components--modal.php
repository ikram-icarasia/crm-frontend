<h4 id="modal" class="push-sm--bottom  soft-lg--top">Modal</h4>

<p>
    Modals are streamlined, but flexible, dialog prompts with the minimum required functionality and smart defaults.
</p>


<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td>
                <!-- Modal trigger -->
                <a href="#" data-toggle="modal" data-target="#modal-name" class="btn  btn--primary">
                    Launch demo modal
                </a>

                <!-- Modal -->
                <div class="modal" id="modal-name" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-head">
                                <h6 class="modal-title">Modal title</h6>
                                <div class="modal-close  icon  icon--close" data-dismiss="modal"></div>
                            </div>
                            <div class="modal-body">
                                <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                            </div>
                            <div class="modal-foot">
                                <button type="button" class="btn" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn--positive">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><!-- Modal trigger -->
<a href="#" data-toggle="modal" data-target="#modal-name">
    Launch demo modal
</a>

<!-- Modal -->
<div class="modal" id="modal-name" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-head">
                <h6 class="modal-title">Modal title</h6>
                <div class="modal-close  icon  icon--close" data-dismiss="modal"></div>
            </div>
            <div class="modal-body">
                <p>Cras mattis consectetur purus sit amet fermentum.</p>
            </div>
            <div class="modal-foot">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn--positive">Save changes</button>
            </div>
        </div>
    </div>
</div></script></code></pre>
