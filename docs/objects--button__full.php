<h4 class="push-sm--bottom">Full Buttons</h4>

<p>
    Set a button to full width by using
    <code>.btn--full</code>.
</p>

<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td colspan="4">
                <button class="btn  btn--full  btn--small">Button</button>
            </td>
        </tr>

        <tr class="milli muted">
            <td class="text--center hard--top" colspan="4">
                Small full width button
            </td>
        </tr>

        <tr>
            <td class="text--center hard" colspan="4">
                <hr class="flush">
            </td>
        </tr>

        <tr>
            <td colspan="4">
                <button class="btn  btn--full">Button</button>
            </td>
        </tr>

        <tr class="milli muted">
            <td class="text--center hard--top" colspan="4">
                Default full width button
            </td>
        </tr>

        <tr>
            <td class="text--center hard" colspan="4">
                <hr class="flush">
            </td>
        </tr>

        <tr>
            <td colspan="4">
                <button class="btn  btn--full  btn--large">Button</button>
            </td>
        </tr>

        <tr class="milli muted">
            <td class="text--center hard--top" colspan="4">
                Large full width button
            </td>
        </tr>

        <tr>
            <td class="text--center hard" colspan="4">
                <hr class="flush">
            </td>
        </tr>

        <tr>
            <td colspan="4">
                <button class="btn  btn--full  btn--huge">Button</button>
            </td>
        </tr>

        <tr class="milli muted">
            <td class="text--center hard--top" colspan="4">
                Huge full width button
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><button class="btn  btn--full  btn--small">Button</button>
<button class="btn  btn--full">Button</button>
<button class="btn  btn--full  btn--large">Button</button>
<button class="btn  btn--full  btn--huge">Button</button></script></code></pre>
