<h4 class="push-sm--bottom">Alignment classes</h4>

<p>Left, right or center align text with text alignment classes.</p>

<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td>
                <div class="text--left">
                    Pack my box with five dozen liquor jugs

                    <div class="milli">
                        <code class="code-css">.text--left</code>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td class="hard">
                <hr>
            </td>
        </tr>

        <tr>
            <td>
                <div class="text--center">
                    Pack my box with five dozen liquor jugs

                    <div class="milli">
                        <code class="code-css">.text--center</code>
                    </div>
                </div>

            </td>
        </tr>

        <tr>
            <td class="hard">
                <hr>
            </td>
        </tr>

        <tr>
            <td>
                <div class="text--right">
                    Pack my box with five dozen liquor jugs

                    <div class="milli">
                        <code class="code-css">.text--right</code>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><div class="text--left"> ... </div>
<div class="text--center"> ... </div>
<div class="text--right"> ... </div></script></code></pre>
