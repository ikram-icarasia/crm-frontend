<h4 class="push-sm--bottom">Button Options</h4>

<p>
    Na na na
</p>


<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td class="one-quarter  text--center">
                <button class="btn">Button</button>
            </td>
            <td class="one-quarter  text--center">
                <button class="btn  btn--positive">Button</button>
            </td>
            <td class="one-quarter  text--center">
                <button class="btn  btn--negative">Button</button>
            </td>
        </tr>

        <tr class="milli muted">
            <td class="text--center hard--top">
                Default
            </td>
            <td class="text--center hard--top">
                Positive
            </td>
            <td class="text--center hard--top">
                Negative
            </td>
        </tr>

        <tr>
            <td class="text--center hard" colspan="3">
                <hr class="flush">
            </td>
        </tr>

        <tr>
            <td class="one-quarter  text--center">
                <button class="btn  btn--primary">Button</button>
            </td>
            <td class="one-quarter  text--center">
                <button class="btn  btn--secondary">Button</button>
            </td>
            <td class="one-quarter  text--center">
                <button class="btn  btn--tertiary">Button</button>
            </td>
        </tr>

        <tr class="milli muted">
            <td class="text--center hard--top">
                Primary
            </td>
            <td class="text--center hard--top">
                Secondary
            </td>
            <td class="text--center hard--top">
                Tertiary
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><button class="btn">Button</button>
<button class="btn  btn--positive">Button</button>
<button class="btn  btn--negative">Button</button>
<button class="btn  btn--primary">Button</button>
<button class="btn  btn--secondary">Button</button>
<button class="btn  btn--tertiary">Button</button></script></code></pre>
