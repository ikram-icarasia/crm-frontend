<h4 class="push-sm--bottom">Font Sizes</h4>

<p>
    The font scale consists of 9 different font sizes. The default size applied to the body is <code class="code-plain">default</code>.
</p>

<div class="documentation-example hard">
    <table class="pack--column  flush">
        <tbody>
            <tr>
                <td>
                    <div class="alpha">Pack my box with five dozen liquor jugs</div>
                    <div class="milli">
                        <code>.alpha</code>

                        <span class="muted">
                            &mdash;
                            Font-size: <span class="alpha-fontsize"></span>px
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="beta">Pack my box with five dozen liquor jugs</div>
                    <div class="milli">
                        <code>.beta</code>

                        <span class="muted">
                            &mdash;
                            Font-size: <span class="beta-fontsize"></span>px
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="gamma">Pack my box with five dozen liquor jugs</div>
                    <div class="milli">
                        <code>.gamma</code>

                        <span class="muted">
                            &mdash;
                            Font-size: <span class="gamma-fontsize"></span>px
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="delta">Pack my box with five dozen liquor jugs</div>
                    <div class="milli">
                        <code>.delta</code>

                        <span class="muted">
                            &mdash;
                            Font-size: <span class="delta-fontsize"></span>px
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="epsilon">Pack my box with five dozen liquor jugs</div>
                    <div class="milli">
                        <code>.epsilon</code>

                        <span class="muted">
                            &mdash;
                            Font-size: <span class="epsilon-fontsize"></span>px
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="zeta">Pack my box with five dozen liquor jugs</div>
                    <div class="milli">
                        <code>.zeta</code>

                        <span class="muted">
                            &mdash;
                            Font-size: <span class="zeta-fontsize"></span>px
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <div>Pack my box with five dozen liquor jugs</div>
                    <div class="milli">
                        <code class="code-plain">default</code>

                        <span class="muted">
                            &mdash;
                            Font-size: <span class="body-fontsize"></span>px
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="milli">Pack my box with five dozen liquor jugs</div>
                    <div class="milli">
                        <code>.milli</code>

                        <span class="muted">
                            &mdash;
                            Font-size: <span class="milli-fontsize"></span>px
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="micro">Pack my box with five dozen liquor jugs</div>
                    <div class="milli">
                        <code>.micro</code>

                        <span class="muted">
                            &mdash;
                            Font-size: <span class="micro-fontsize"></span>px
                        </span>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><div class="alpha"> ... </div>
<div class="beta"> ... </div>
<div class="gamma"> ... </div>
<div class="delta"> ... </div>
<div class="epsilon"> ... </div>
<div class="zeta"> ... </div>
<div> ... </div>
<div class="milli"> ... </div>
<div class="micro"> ... </div></script></code></pre>
