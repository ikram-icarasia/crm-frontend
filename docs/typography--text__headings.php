<h4 class="push-sm--bottom">Headings</h4>

<p>
    When we define a heading we also define a corresponding class to go with it.
</p>

<p>
    This allows us to apply, say, <code>class="alpha"</code> to a <code>h3</code>; a double-stranded
    heading hierarchy.
</p>

<div class="documentation-example hard">
    <table class="pack--column  flush">
        <tbody>
            <tr>
                <td>
                    <h1 class="flush">Heading 1</h1>
                    <div class="milli muted">Font-size: <span class="h1-fontsize"></span>px</div>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <h2 class="flush">Heading 2</h2>
                    <div class="milli muted">Font-size: <span class="h2-fontsize"></span>px</div>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <h3 class="flush">Heading 3</h3>
                    <div class="milli muted">Font-size: <span class="h3-fontsize"></span>px</div>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <h4 class="flush">Heading 4</h4>
                    <div class="milli muted">Font-size: <span class="h4-fontsize"></span>px</div>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <h5 class="flush">Heading 5</h5>
                    <div class="milli muted">Font-size: <span class="h5-fontsize"></span>px</div>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <h6 class="flush">Heading 6</h6>
                    <div class="milli muted">Font-size: <span class="h6-fontsize"></span>px</div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<h5>Heading 5</h5>
<h6>Heading 6</h6></script></code></pre>
