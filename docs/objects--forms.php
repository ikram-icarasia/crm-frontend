<h4 class="push-sm--bottom">Text Inputs</h4>

<p>Instead of a <code>[type]</code> selector for each kind of form input, we just use a class to target any/every one</p>

<table class="pack--modifier">
    <thead>
        <tr>
            <th class="one-third">Modifier</th>
            <th class="two-third">Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="one-third">
                <code class="code-attr">:disabled</code>
            </td>
            <td class="two-third">
                Disabled
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-attr">:readonly</code>
            </td>
            <td class="two-third">
                Read only
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-css">.input--success</code>
            </td>
            <td class="two-third">
                Success intent
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-css">.input--error</code>
            </td>
            <td class="two-third">
                Error intent
            </td>
        </tr>
    </tbody>
</table>

<div class="documentation-example hard">
    <table class="pack--column flush">
        <tbody>
            <tr>
                <td>
                    <p><code class="code-plain">default</code></p>
                    <input type="text" class="input" placeholder="Placeholder Text">
                </td>
                <td>
                    <p><code class="code-attr">:disabled</code></p>
                    <input type="text" class="input" placeholder="Placeholder Text" disabled="">
                </td>
                <td>
                    <p><code class="code-attr">:readonly</code></p>
                    <input type="text" class="input" placeholder="Placeholder Text" readonly="">
                </td>
            </tr>

            <tr>
                <td class="hard" colspan="3">
                    <hr class="flush">
                </td>
            </tr>

            <tr>
                <td>
                    <p><code class="code-class">.input--success</code></p>
                    <input type="text" class="input  input--success" placeholder="Placeholder Text">
                </td>
                <td>
                    <p><code class="code-class">.input--warning</code></p>
                    <input type="text" class="input  input--warning" placeholder="Placeholder Text">
                </td>
                <td>
                    <p><code class="code-class">.input--error</code></p>
                    <input type="text" class="input  input--error" placeholder="Placeholder Text">
                </td>
            </tr>
        </tbody>
    </table>
</div>
<pre class="flush"><code class="language-markup  line-numbers"><script type="prism-html-markup"><input type="text" placeholder="Placeholder Text" class="input">
<input type="text" placeholder="Placeholder Text" class="input" disabled="">
<input type="text" placeholder="Placeholder Text" class="input" readonly="">
<input type="text" placeholder="Placeholder Text" class="input  input--success">
<input type="text" placeholder="Placeholder Text" class="input  input--warning">
<input type="text" placeholder="Placeholder Text" class="input  input--error"></script></code></pre>
