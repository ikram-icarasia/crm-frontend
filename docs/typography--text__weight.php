<h4 class="push-sm--bottom">Font Weights</h4>

<p>There are four font weights available to use — light, regular semibold, and bold. </p>

<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td class="one-quarter  text--center  hard--bottom">
                <div class="alpha  text--light">
                    Aa
                </div>
            </td>
            <td class="one-quarter  text--center  hard--bottom">
                <div class="alpha  text--normal">
                    Aa
                </div>
            </td>
            <td class="one-quarter  text--center  hard--bottom">
                <div class="alpha  text--semibold">
                    Aa
                </div>
            </td>
            <td class="one-quarter  text--center  hard--bottom">
                <div class="alpha  text--bold">
                    Aa
                </div>
            </td>
        </tr>

        <tr class="milli muted">
            <td class="text--center hard--top">
                Light
            </td>
            <td class="text--center hard--top">
                Normal
            </td>
            <td class="text--center hard--top">
                Semibold
            </td>
            <td class="text--center hard--top">
                Bold
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><div class="text--light"> ... </div>
<div class="text--normal"> ... </div>
<div class="text--semibold"> ... </div>
<div class="text--bold"> ... </div></script></code></pre>
