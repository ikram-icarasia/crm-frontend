<h4 class="push-sm--bottom">Disabled/Inactive Buttons</h4>

<p>
    Na na na
</p>


<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td class="one-quarter  text--center">
                <button class="btn btn--inactive">Button</button>
            </td>
            <td class="one-quarter  text--center">
                <button class="btn btn--inactive  btn--positive">Button</button>
            </td>
            <td class="one-quarter  text--center">
                <button class="btn btn--inactive  btn--negative">Button</button>
            </td>
        </tr>

        <tr class="milli muted">
            <td class="text--center hard--top">
                Default
            </td>
            <td class="text--center hard--top">
                Positive
            </td>
            <td class="text--center hard--top">
                Negative
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><button class="btn btn--inactive">Button</button>
<button class="btn btn--inactive  btn--positive">Button</button>
<button class="btn btn--inactive  btn--negative">Button</button></script></code></pre>


<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td class="one-quarter  text--center">
                <button class="btn  btn--inactive  btn--primary">Button</button>
            </td>
            <td class="one-quarter  text--center">
                <button class="btn  btn--inactive  btn--secondary">Button</button>
            </td>
            <td class="one-quarter  text--center">
                <button class="btn  btn--inactive  btn--tertiary">Button</button>
            </td>
        </tr>

        <tr class="milli muted">
            <td class="text--center hard--top">
                Primary
            </td>
            <td class="text--center hard--top">
                Secondary
            </td>
            <td class="text--center hard--top">
                Tertiary
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><button class="btn  btn--inactive  btn--primary">Button</button>
<button class="btn  btn--inactive  btn--secondary">Button</button>
<button class="btn  btn--inactive  btn--tertiary">Button</button></script></code></pre>
