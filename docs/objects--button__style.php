<h4 class="push-sm--bottom">Button Styles</h4>

<p>
    Set button style by using <code>.btn--soft</code> or <code>.btn--hard</code>
</p>


<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td class="text--center t50">
                <button class="btn  btn--soft">Soft Button</button>
            </td>
            <td class="text--center t50">
                <button class="btn  btn--hard">Hard Button</button>
            </td>
        </tr>

        <tr class="milli muted">
            <td class="text--center hard--top">
                Soft button
            </td>
            <td class="text--center hard--top">
                Hard button
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><button class="btn  btn--soft">Button</button>
<button class="btn  btn--hard">Button</button></script></code></pre>
