<h4 class="push-sm--bottom">Typeface</h4>

<p>
    Primary typeface is <code class="body-fontfamily"></code> — a robust typeface with legible numbers that renders well at all sizes. It is soft and friendly, yet plain enough to get out of the way and let the user's designs shine.
</p>

<div class="documentation-example  documentation-fonts  is--solo  hard  visuallyhidden">
    <table class="pack--column  flush">
        <tbody>
            <tr>
                <td class="text--uppercase">
                    Pack my box with five dozen liquor jugs
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td class="text--uppercase">
                    <strong>
                        Pack my box with five dozen liquor jugs
                    </strong>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    pack my box with five dozen liquor jugs
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <strong>
                        pack my box with five dozen liquor jugs
                    </strong>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <span>0</span>
                    <span>1</span>
                    <span>2</span>
                    <span>3</span>
                    <span>4</span>
                    <span>5</span>
                    <span>6</span>
                    <span>7</span>
                    <span>8</span>
                    <span>9</span>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <strong>
                        <span>0</span>
                        <span>1</span>
                        <span>2</span>
                        <span>3</span>
                        <span>4</span>
                        <span>5</span>
                        <span>6</span>
                        <span>7</span>
                        <span>8</span>
                        <span>9</span>
                    </strong>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    ​‌<span>‘</span>
                    ​‌<span>?</span>
                    ​‌<span>’</span>
                    ​‌<span>“</span>
                    ​‌<span>!</span>
                    ​‌<span>”</span>
                    ​‌<span>(</span>
                    ​‌<span>%</span>
                    ​‌<span>)</span>
                    ​‌<span>[</span>
                    ​‌<span>#</span>
                    ​‌<span>]</span>
                    ​‌<span>{</span>
                    ​‌<span>@</span>
                    ​‌<span>}</span>
                    ​‌<span>/</span>
                    ​‌<span>&</span>
                    ​‌<span><</span>
                    ​‌<span>-</span>
                    ​‌<span>+​</span>
                    ‌<span>÷</span>
                    ​‌<span>×</span>
                    ​‌<span>=</span>
                    ​‌<span>></span>
                    ​‌<span>®</span>
                    ​‌<span>©</span>
                    ​‌<span>$</span>
                    ​‌<span>£</span>
                    ​‌<span>¥</span>
                    ​‌<span>¢</span>
                    ​‌<span>:</span>
                    ​‌<span>;</span>
                    ​‌<span>,</span>
                    ​‌<span>.</span>
                    ​‌<span>*</span>
                </td>
            </tr>

            <tr>
                <td class="hard">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>
                    <strong>
                        ​‌<span>‘</span>
                        ​‌<span>?</span>
                        ​‌<span>’</span>
                        ​‌<span>“</span>
                        ​‌<span>!</span>
                        ​‌<span>”</span>
                        ​‌<span>(</span>
                        ​‌<span>%</span>
                        ​‌<span>)</span>
                        ​‌<span>[</span>
                        ​‌<span>#</span>
                        ​‌<span>]</span>
                        ​‌<span>{</span>
                        ​‌<span>@</span>
                        ​‌<span>}</span>
                        ​‌<span>/</span>
                        ​‌<span>&</span>
                        ​‌<span><</span>
                        ​‌<span>-</span>
                        ​‌<span>+​</span>
                        ‌<span>÷</span>
                        ​‌<span>×</span>
                        ​‌<span>=</span>
                        ​‌<span>></span>
                        ​‌<span>®</span>
                        ​‌<span>©</span>
                        ​‌<span>$</span>
                        ​‌<span>£</span>
                        ​‌<span>¥</span>
                        ​‌<span>¢</span>
                        ​‌<span>:</span>
                        ​‌<span>;</span>
                        ​‌<span>,</span>
                        ​‌<span>.</span>
                        ​‌<span>*</span>
                    </strong>
                </td>
            </tr>
        </tbody>
    </table>
</div>
