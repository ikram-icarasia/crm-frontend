<h4 class="push-sm--bottom">Grids</h4>

<p>Fluid and nestable grid system.</p>


<div class="documentation-example hard">
    <table class="pack--column flush text--left">
        <tr>
            <td>
                <div class="grid">
                    <div class="grid__item  one-whole  push-md--bottom">
                        <div class="greybox">.one-whole</div>
                    </div>

                    <div class="grid__item  one-half  push-md--bottom">
                        <div class="greybox">.one-half</div>
                    </div>

                    <div class="grid__item  one-half  push-md--bottom">
                        <div class="greybox">.one-half</div>
                    </div>

                    <div class="grid__item  one-third">
                        <div class="greybox">.one-third</div>
                    </div>

                    <div class="grid__item  two-thirds">
                        <div class="greybox">.two-thirds</div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
<pre class="flush"><code class="language-markup  line-numbers"><script type="prism-html-markup"><div class="grid">
    <div class="grid__item  one-whole">
        .one-whole
    </div>

    <div class="grid__item  one-half">
        .one-half
    </div>

    <div class="grid__item  one-half">
        .one-half
    </div>

    <div class="grid__item  one-third">
        .one-third
    </div>

    <div class="grid__item  two-thirds">
        .two-thirds
    </div>
</div></script></code></pre>

<hr>

<h4 class="push-sm--bottom">Grids Full</h4>

<p>No spacings grid layout.</p>

<div class="documentation-example hard">
    <table class="pack--column flush text--left">
        <tr>
            <td>
                <div class="grid  grid--full">
                    <div class="grid__item  one-whole  push-md--bottom">
                        <div class="greybox">.one-whole</div>
                    </div>

                    <div class="grid__item  one-half  push-md--bottom">
                        <div class="greybox">.one-half</div>
                    </div>

                    <div class="grid__item  one-half  push-md--bottom">
                        <div class="greybox">.one-half</div>
                    </div>

                    <div class="grid__item  one-third">
                        <div class="greybox">.one-third</div>
                    </div>

                    <div class="grid__item  two-thirds">
                        <div class="greybox">.two-thirds</div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
<pre class="flush"><code class="language-markup  line-numbers"><script type="prism-html-markup"><div class="grid  grid--full">
    <div class="grid__item  one-whole">
        .one-whole
    </div>

    <div class="grid__item  one-half">
        .one-half
    </div>

    <div class="grid__item  one-half">
        .one-half
    </div>

    <div class="grid__item  one-third">
        .one-third
    </div>

    <div class="grid__item  two-thirds">
        .two-thirds
    </div>
</div></script></code></pre>
