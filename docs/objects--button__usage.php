<h4 class="push-sm--bottom">Button Usage</h4>

<p>
    Use the button classes on an <code>&lt;a&gt;</code>, <code>&lt;button&gt;</code>, or <code>&lt;input&gt;</code> element.
</p>


<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td class="text--center one-third">
                <a class="btn" href="#" role="button">Link</a>
            </td>
            <td class="text--center one-third">
                <button class="btn" type="submit">Button</button>
            </td>
            <td class="text--center one-third">
                <input class="btn" type="button" value="Input">
            </td>
        </tr>

        <tr class="milli muted">
            <td class="text--center hard--top">
                Link
            </td>
            <td class="text--center hard--top">
                Button
            </td>
            <td class="text--center hard--top">
                Input
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><a class="btn btn-default" href="#" role="button">Link</a>
<button class="btn" type="submit">Button</button>
<input class="btn" type="button" value="Input">
<input class="btn" type="submit" value="Submit"></script></code></pre>
