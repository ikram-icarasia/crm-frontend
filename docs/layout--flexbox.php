<h4 class="push-sm--bottom">pack</h4>

<p>Until we can utilise pack natively we can kinda, sorta, attempt to emulate it, in a way..</p>


<div class="documentation-example hard">
    <table class="pack--column flush text--left">
        <tr>
            <td>
                <p class="muted  text--uppercase  milli">Vertical align middle (Default)</p>
                <div class="pack  text--center">
                    <div class="pack__item  one-half">
                        <div class="greenbox" style="border-radius: 0; display: block">Small box</div>
                    </div>

                    <div class="pack__item  one-half">
                        <div class="greenbox" style="border-radius: 0; display: block; padding: 3em 0">Bigger Box</div>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td>
                <hr class="flush">
            </td>
        </tr>

        <tr>
            <td>
                <p class="muted  text--uppercase  milli">Vertical align top</p>

                <div class="pack  text--center">
                    <div class="pack__item  valign--top  one-half">
                        <div class="greenbox" style="border-radius: 0; display: block">Small box</div>
                    </div>

                    <div class="pack__item  one-half">
                        <div class="greenbox" style="border-radius: 0; display: block; padding: 3em 0">Bigger Box</div>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td>
                <hr class="flush">
            </td>
        </tr>

        <tr>
            <td>
                <p class="muted  text--uppercase  milli">Vertical align bottom</p>
                <div class="pack  text--center">
                    <div class="pack__item  valign--bottom  one-half">
                        <div class="greenbox" style="border-radius: 0; display: block">Small box</div>
                    </div>

                    <div class="pack__item  one-half">
                        <div class="greenbox" style="border-radius: 0; display: block; padding: 3em 0">Bigger Box</div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
<pre class="flush"><code class="language-markup  line-numbers"><script type="prism-html-markup"><div class="pack">
    <div class="pack__item">
        <div class="greenbox">Small box</div>
    </div>

    <div class="pack__item">
        <div class="greenbox">Bigger Box</div>
    </div>
</div>

<div class="pack">
    <div class="pack__item  valign--top">
        <div class="greenbox">Small box</div>
    </div>

    <div class="pack__item">
        <div class="greenbox">Bigger Box</div>
    </div>
</div>

<div class="pack">
    <div class="pack__item  valign--bottom">
        <div class="greenbox">Small box</div>
    </div>

    <div class="pack__item">
        <div class="greenbox">Bigger Box</div>
    </div>
</div></script></code></pre>
