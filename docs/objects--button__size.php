<h4 class="push-sm--bottom">Button Sizes</h4>

<p>
    Define a button size with
    <code>.btn--small</code>,
    <code>.btn--large</code>,
    or
    <code>.btn--huge</code>.
</p>


<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td class="one-quarter  text--center">
                <button class="btn  btn--small">Button</button>
            </td>
            <td class="one-quarter  text--center">
                <button class="btn">Button</button>
            </td>
            <td class="one-quarter  text--center">
                <button class="btn  btn--large">Button</button>
            </td>
            <td class="one-quarter  text--center">
                <button class="btn  btn--huge">Buttons</button>
            </td>
        </tr>

        <tr class="milli muted">
            <td class="text--center hard--top">
                Small button
            </td>
            <td class="text--center hard--top">
                Default button
            </td>
            <td class="text--center hard--top">
                Large button
            </td>
            <td class="text--center hard--top">
                Huge button
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><button class="btn  btn--small">Button</button>
<button class="btn">Button</button>
<button class="btn  btn--large">Button</button>
<button class="btn  btn--huge">Button</button></script></code></pre>
