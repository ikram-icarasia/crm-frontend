

<h4 class="push-sm--bottom">Radios</h4>

<p>
    We use custom styling for radio. You should then wrap the whole thing in a <code class="code-plain">&lt;label&gt;</code> with the classes <code>.radio</code>.
</p>

<table class="pack--modifier">
    <thead>
        <tr>
            <th class="one-third">Modifier</th>
            <th class="two-third">Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="one-third">
                <code class="code-attr">:checked</code>
            </td>
            <td class="two-third">
                Checked state
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-attr">:disabled</code>
            </td>
            <td class="two-third">
                Disabled state
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-css">.radio--inline</code>
            </td>
            <td class="two-third">
                Inline radio
            </td>
        </tr>
    </tbody>
</table>









<div class="documentation-example hard">
    <table class="pack--column flush">
        <tbody>
            <tr>
                <td>
                    <p><code class="code-plain">default</code></p>
                    <label class="radio">
                        <input type="radio" name="doc-radio">
                        <span class="icon"></span>
                        Radio
                    </label>
                </td>
                <td>
                    <p><code class="code-attr">:checked</code></p>
                    <label class="radio">
                        <input type="radio" name="doc-radio" checked="">
                        <span class="icon"></span>
                        Radio
                    </label>
                </td>
                <td>
                    <p><code class="code-attr">:disabled</code></p>
                    <label class="radio">
                        <input type="radio" name="doc-radio" disabled="">
                        <span class="icon"></span>
                        Radio
                    </label>
                </td>
            </tr>

            <tr>
                <td class="hard" colspan="3">
                    <hr class="flush">
                </td>
            </tr>

            <tr>
                <td colspan="3">
                    <p><code class="code-css">.radio-inline</code></p>
                    <label class="radio  radio--inline  push-md--right">
                        <input type="radio" name="doc-radio--inline">
                        <span class="icon"></span>
                        Radio
                    </label>

                    <label class="radio  radio--inline  push-md--right">
                        <input type="radio" name="doc-radio--inline" checked="">
                        <span class="icon"></span>
                        Radio
                    </label>

                    <label class="radio  radio--inline  push-md--right">
                        <input type="radio" name="doc-radio--inline" disabled="">
                        <span class="icon"></span>
                        Radio
                    </label>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<pre class="flush"><code class="language-markup  line-numbers"><script type="prism-html-markup"><label class="radio">
    <input type="radio" name="doc-radio">
    <span class="icon"></span>
    Radio
</label>

<label class="radio">
    <input type="radio" name="doc-radio" checked="">
    <span class="icon"></span>
    Radio
</label>

<label class="radio">
    <input type="radio" name="doc-radio" disabled="">
    <span class="icon"></span>
    Radio
</label>

<label class="radio  radio--inline  push-md--right">
    <input type="radio" name="doc-radio--inline">
    <span class="icon"></span>
    Radio
</label>
<label class="radio  radio--inline  push-md--right">
    <input type="radio" name="doc-radio--inline" checked="">
    <span class="icon"></span>
    Radio
</label>
<label class="radio  radio--inline  push-md--right">
    <input type="radio" name="doc-radio--inline" disabled="">
    <span class="icon"></span>
    Radio
</label></script></code></pre>
