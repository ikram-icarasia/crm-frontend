<h4 class="push-sm--bottom">Floats</h4>

<p>Float elements by using this float classes, and also to remove the float too.</p>


<div class="documentation-example hard">
    <table class="pack--column flush text--left">
        <tr>
            <td colspan="4">
                <span class="greybox  float--none">Float none</span>
            </td>
        </tr>

        <tr>
            <td class="text--center hard" colspan="4">
                <hr class="flush">
            </td>
        </tr>

        <tr>
            <td colspan="4">
                <span class="greybox  float--right">Float right</span>
            </td>
        </tr>

        <tr>
            <td class="text--center hard" colspan="4">
                <hr class="flush">
            </td>
        </tr>

        <tr>
            <td colspan="4">
                <span class="greybox  float--left">Float left</span>
            </td>
        </tr>
    </table>
</div>
<pre class="flush"><code class="language-markup  line-numbers"><script type="prism-html-markup"><span class="float--none"></span>
<span class="float--right"></span>
<span class="float--left"></span></script></code></pre>
