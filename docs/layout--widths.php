<h4 class="push-sm--bottom">Widths</h4>

<p> 
    Sizes in human readable format. These are used in conjunction with other objects and abstractions found in inuit.css, most commonly the grid system and faux pack.
</p>

<p>
    We have a mixin to generate our widths and their breakpoint-specific variations.
</p>

<table class="pack--modifier">
    <thead>
        <tr>
            <th class="one-third">Modifier</th>
            <th class="two-third">Width</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2" class="milli muted  text--uppercase">
                Whole
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.one-whole</code>
            </td>
            <td class="text--number  two-third">
                100%
            </td>
        </tr>

        <tr>
            <td colspan="2" class="milli muted  text--uppercase  soft-lg--top">
                Halves
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.one-half</code>
            </td>
            <td class="text--number  two-third">
                50%
            </td>
        </tr>

        <tr>
            <td colspan="2" class="milli muted  text--uppercase  soft-lg--top">
                Thirds
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.one-third</code>
            </td>
            <td class="text--number  two-third">
                33.333%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.two-thirds</code>
            </td>
            <td class="text--number  two-third">
                66.666%
            </td>
        </tr>



        <tr>
            <td colspan="2" class="milli muted  text--uppercase  soft-lg--top">
                Quarters
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.one-quarter</code>
            </td>
            <td class="text--number  two-third">
                25%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.two-quarters</code>
            </td>
            <td class="text--number  two-third">
                50%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.three-quarters</code>
            </td>
            <td class="text--number  two-third">
                75%
            </td>
        </tr>



        <tr>
            <td colspan="2" class="milli muted  text--uppercase  soft-lg--top">
                Fifths
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.one-quarter</code>
            </td>
            <td class="text--number  two-third">
                20%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.two-quarters</code>
            </td>
            <td class="text--number  two-third">
                40%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.three-quarters</code>
            </td>
            <td class="text--number  two-third">
                60%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.four-quarters</code>
            </td>
            <td class="text--number  two-third">
                80%
            </td>
        </tr>




        <tr>
            <td colspan="2" class="milli muted  text--uppercase  soft-lg--top">
                Sixth
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.one-sixth</code>
            </td>
            <td class="text--number  two-third">
                16.666%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.two-sixth</code>
            </td>
            <td class="text--number  two-third">
                33.333%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.three-sixth</code>
            </td>
            <td class="text--number  two-third">
                50%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.four-sixth</code>
            </td>
            <td class="text--number  two-third">
                66.666%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.five-sixth</code>
            </td>
            <td class="text--number  two-third">
                83.333%
            </td>
        </tr>




        <tr>
            <td colspan="2" class="milli muted  text--uppercase  soft-lg--top">
                Eighths xxx
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.one-eighth</code>
            </td>
            <td class="text--number  two-third">
                12.5%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.two-eighths</code>
            </td>
            <td class="text--number  two-third">
                25%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.three-eighths</code>
            </td>
            <td class="text--number  two-third">
                37.5%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.four-eighths</code>
            </td>
            <td class="text--number  two-third">
                50%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.five-eighths</code>
            </td>
            <td class="text--number  two-third">
                62.5%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.six-eighths</code>
            </td>
            <td class="text--number  two-third">
                75%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.seven-eighths</code>
            </td>
            <td class="text--number  two-third">
                87.5%
            </td>
        </tr>




        <tr>
            <td colspan="2" class="milli muted  text--uppercase  soft-lg--top">
                Tenths
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.one-tenth</code>
            </td>
            <td class="text--number  two-third">
                10%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.two-tenths</code>
            </td>
            <td class="text--number  two-third">
                20%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.three-tenths</code>
            </td>
            <td class="text--number  two-third">
                30%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.four-tenths</code>
            </td>
            <td class="text--number  two-third">
                40%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.five-tenths</code>
            </td>
            <td class="text--number  two-third">
                50%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.six-tenths</code>
            </td>
            <td class="text--number  two-third">
                60%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.seven-tenths</code>
            </td>
            <td class="text--number  two-third">
                70%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.eight-tenths</code>
            </td>
            <td class="text--number  two-third">
                80%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.nine-tenths</code>
            </td>
            <td class="text--number  two-third">
                90%
            </td>
        </tr>




        <tr>
            <td colspan="2" class="milli muted  text--uppercase  soft-lg--top">
                Twelfth
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.one-twelfth</code>
            </td>
            <td class="text--number  two-third">
                8.333%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.two-twelfths</code>
            </td>
            <td class="text--number  two-third">
                16.666%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.three-twelfths</code>
            </td>
            <td class="text--number  two-third">
                25%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.four-twelfths</code>
            </td>
            <td class="text--number  two-third">
                33.333%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.five-twelfths</code>
            </td>
            <td class="text--number  two-third">
                41.666%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.six-twelfths</code>
            </td>
            <td class="text--number  two-third">
                50%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.seven-twelfths</code>
            </td>
            <td class="text--number  two-third">
                58.333%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.eight-twelfths</code>
            </td>
            <td class="text--number  two-third">
        	    58.333%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.nine-twelfths</code>
            </td>
            <td class="text--number  two-third">
                66.666%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.ten-twelfths</code>
            </td>
            <td class="text--number  two-third">
                83.333%
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.eleven-twelfths</code>
            </td>
            <td class="text--number  two-third">
                91.666%
            </td>
        </tr>

























        <tr>
            <td colspan="2" class="milli muted  text--uppercase  soft-lg--top">
                Responsive
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-css">.palm-[width]</code>
            </td>
            <td class="two-third">
                Set width in <code class="code-css">palm-</code> breakpoint
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-css">.lap-[width]</code>
            </td>
            <td class="two-third">
                Set width in <code class="code-css">lap-</code> breakpoint
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-css">.lap-and-up-[width]</code>
            </td>
            <td class="two-third">
                Set width in <code class="code-css">lap-and-up-</code> breakpoint
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-css">.portable-[width]</code>
            </td>
            <td class="two-third">
                Set width in <code class="code-css">portable-</code> breakpoint
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-css">.desk-[width]</code>
            </td>
            <td class="two-third">
                Set width in <code class="code-css">desk-</code> breakpoint
            </td>
        </tr>
    </tbody>
</table>
