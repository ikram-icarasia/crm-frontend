<h4 id="tab" class="push-sm--bottom soft-lg--top">Tabs</h4>

<p>
    The tabbed navigation module allows you to easily create tabbed navigation systems, that are fully customizable to fit your needs.
</p>

<p>
    Nested tabs are not supported and might cause unexpected behavior.
</p>


<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td>
                <ul class="tabs">
                    <li class="tab active">
                        <a href="#tab-1" data-toggle="tab">Tab 1</a>
                    </li>
                    <li class="tab">
                        <a href="#tab-2" data-toggle="tab">Tab 2</a>
                    </li>
                    <li class="tab">
                        <a href="#tab-3" data-toggle="tab">Tab 3</a>
                    </li>
                </ul>

                <div class="tabs-content">
                    <div id="tab-1" class="tab-pane  active">
                        Liquorice lollipop icing jelly-o brownie. Halvah wafer donut pudding icing chupa chups. Marzipan cupcake chocolate cake soufflé pastry cake cheesecake lemon drops jelly. Caramels chocolate bar jelly croissant gingerbread gingerbread topping.
                    </div>
                    <div id="tab-2" class="tab-pane">
                        Jelly-o marzipan cupcake pudding biscuit tart tootsie roll. Lollipop pie carrot cake chocolate cake bear claw sugar plum. Dessert tootsie roll chocolate cake bonbon fruitcake brownie lollipop chocolate.
                    </div>
                    <div id="tab-3" class="tab-pane">
                        Powder lollipop soufflé powder. Jelly gummi bears soufflé chocolate pastry. Icing liquorice gingerbread apple pie carrot cake cotton candy tiramisu cookie marshmallow.
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><!--Tab menu-->
<ul class="tab-menu">
    <li class="tab-menu__item active">
        <a class="tab-menu__link" href="#tab-1" data-toggle="tab">Tab 1</a>
    </li>
    <li class="tab-menu__item">
        <a class="tab-menu__link" href="#tab-2" data-toggle="tab">Tab 2</a>
    </li>
    <li class="tab-menu__item">
        <a class="tab-menu__link" href="#tab-3" data-toggle="tab">Tab 3</a>
    </li>
</ul>

<!--Tab contents-->
<div class="tab-content">
    <div id="tab-1" class="tab-content__item active">
        Liquorice lollipop icing jelly-o brownie. Halvah wafer donut pudding icing chupa chups. Marzipan cupcake chocolate cake soufflé pastry cake cheesecake lemon drops jelly. Caramels chocolate bar jelly croissant gingerbread gingerbread topping.
    </div>
    <div id="tab-2" class="tab-content__item">
        Jelly-o marzipan cupcake pudding biscuit tart tootsie roll. Lollipop pie carrot cake chocolate cake bear claw sugar plum. Dessert tootsie roll chocolate cake bonbon fruitcake brownie lollipop chocolate.
    </div>
    <div id="tab-3" class="tab-content__item">
        Powder lollipop soufflé powder. Jelly gummi bears soufflé chocolate pastry. Icing liquorice gingerbread apple pie carrot cake cotton candy tiramisu cookie marshmallow.
    </div>
</div></script></code></pre>
