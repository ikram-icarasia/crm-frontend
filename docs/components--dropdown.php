<h4 id="dropdown" class="push-sm--bottom  soft-lg--top">Dropdown</h4>

<p>
    Modals are streamlined, but flexible, dialog prompts with the minimum required functionality and smart defaults.
</p>


<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td>
                <div class="dropdown">
                    <button data-toggle="dropdown" class="btn btn--secondary">
                        Dropdown trigger
                    </button>
                    <div class="dropdown__menu">
                        <ul class="dropdown__list">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><div class="dropdown">
    <button data-toggle="dropdown" class="btn btn--secondary">
        Dropdown trigger
    </button>
    <div class="dropdown__menu">
        <ul class="dropdown__list">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
        </ul>
    </div>
</div></script></code></pre>
