<h4 class="push-sm--bottom">Text Truncate</h4>

<p>Truncate longer one line text properly with ellipsis.</p>

<pre><code class="language-css  line-numbers">.text--truncate     { @include truncate(100%) }</code></pre>
