<h2 class="push-sm--bottom">Documentation</h2>

<p>
    Naming things is hard but worth getting right. To make it somewhat simpler we use the BEM methodology within class names. BEM also helps us to avoid cross module collisions and to signify intent and relationships from the classnames.
</p>

<p>
    Read more about BEM here: <a href="http://getbem.com/introduction/">http://getbem.com/introduction/</a>
</p>

<p>
    Our version of BEM follows this syntax:
</p>

<ul>
    <li><code>.block</code>: lowercase, with dashes if needed</li>
    <li><code>.block__element</code></li>
    <li><code>.block--modifier</code>: only to be used in combination with a <code>.block</code> or <code>.block__element</code></li>
</ul>

<p>
    For example:
</p>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><div class="block  block--modifier">
    <h1 class="block__title">Gallery</h1>
    <img class="block__image  block__image--modifier" />
    <img class="block__image" />
    <img class="block__image" />
</div></script></code></pre>

<p>
    Note that we used double spacing between classes name.
</p>



<hr>



<h4 class="push-sm--bottom">Folder Structure</h4>

<p>Need some insights on folder structure.</p>



<hr>



<h4 class="push-sm--bottom">Don'ts</h4>

<ul>
<li>Avoid using HTML tags in CSS selectors

<ul>
<li>E.g. Prefer <code>.modal {}</code> over <code>div.modal {}</code></li>
<li>Always prefer using a class over HTML tags (with some exceptions like CSS resets)</li>
</ul></li>
<li>Don't use ids in selectors

<ul>
<li><code>#header</code> is overly specific compared to, for example <code>.header</code> and is much harder to override</li>
<li>Read more about the headaches associated with IDs in CSS <a href="http://csswizardry.com/2011/09/when-using-ids-can-be-a-pain-in-the-class/">here</a>.</li>
</ul></li>
<li>Don’t nest more than 3 levels deep

<ul>
<li>Nesting selectors increases specificity, meaning that overriding any CSS set therein needs to be targeted with an even more specific selector. This quickly becomes a significant maintenance issue.</li>
</ul></li>
<li>Avoid using nesting for anything other than pseudo selectors and state selectors.

<ul>
<li>E.g. nesting <code>:hover</code>, <code>:focus</code>, <code>::before</code>, etc. is OK, but nesting selectors inside selectors should be avoided.</li>
</ul></li>
<li>Don't <code>!important</code>

<ul>
<li>Ever.</li>
<li>If you must, leave a comment, and prioritise resolving specificity issues before resorting to <code>!important</code>.</li>
<li><code>!important</code> greatly increases the power of a CSS declaration, making it extremely tough to override in the future. It’s only possible to override with another <code>!important</code> declaration later in the cascade.</li>
</ul></li>
<li>Avoid shorthand properties (unless you really need them)

<ul>
<li>It can be tempting to use, for instance, <code>background: #fff</code> instead of <code>background-color: #fff</code>, but doing so overrides other values encapsulated by the shorthand property. (In this case, <code>background-image</code> and its associative properties are set to “none.”</li>
<li>This applies to all properties with a shorthand: border, margin, padding, font, etc.</li>
</ul></li>
</ul>



<h4 class="text--semibold push-sm--bottom">Spacing</h4>

<ul>
<li>Four spaces for indenting code</li>
<li>Put spaces after <code>:</code> in property declarations

<ul>
<li>E.g. <code>color: red;</code> instead of <code>color:red;</code></li>
</ul></li>
<li>Put spaces before <code>{</code> in rule declarations

<ul>
<li>E.g. <code>.modal {</code> instead of <code>.modal{</code></li>
</ul></li>
<li>Write your CSS one line per property</li>
<li>Add a line break after <code>}</code> closing rule declarations</li>
<li>When grouping selectors, keep individual selectors on a single line</li>
<li>Place closing braces <code>}</code> on a new line</li>
<li>Add a new line at the end of .scss files</li>
<li>Trim excess whitespace</li>
</ul>



<h4 class="text--semibold push-sm--bottom">Formatting</h4>

<ul>
<li>All selectors are lower case, hyphen separated aka “spinal case” eg. <code>.my-class-name</code></li>
<li>Always prefer Sass’s double-slash <code>//</code> commenting, even for block comments</li>
<li>Avoid specifying units for zero values, e.g. <code>margin: 0;</code> instead of <code>margin: 0px;</code></li>
<li>Always add a semicolon to the end of a property/value declaration</li>
<li>Use leading zeros for decimal values <code>opacity: 0.4;</code> instead of <code>opacity: .4;</code></li>
<li>Put spaces before and after child selector <code>div &gt; span</code> instead of <code>div&gt;span</code></li>
</ul>
