<h4 class="push-sm--bottom">Text Cases</h4>

<p>Transform text with text transformation classes.</p>

<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td>
                <div>
                    Pack my Box with five dozen liquor jugs
                </div>
                <div class="milli">
                    <code class="code-plain">default</code>
                </div>
            </td>
        </tr>

        <tr>
            <td class="hard">
                <hr>
            </td>
        </tr>

        <tr>
            <td>
                <div class="text--uppercase">
                    Pack my Box with five dozen liquor jugs
                </div>
                <div class="milli">
                    <code class="code-css">.text--uppercase</code>
                </div>
            </td>
        </tr>

        <tr>
            <td class="hard">
                <hr>
            </td>
        </tr>

        <tr>
            <td>
                <div class="text--lowercase">
                    Pack my Box with five dozen liquor jugs
                </div>
                <div class="milli">
                    <code class="code-css">.text--lowercase</code>
                </div>
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><div class="text--uppercase"> ... </div>
<div class="text--lowercase"> ... </div></script></code></pre>
