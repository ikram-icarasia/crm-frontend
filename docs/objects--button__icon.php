<h4 class="push-sm--bottom">Button Icon</h4>

<p>
    Enhance buttons with icons.
</p>


<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td class="one-quarter  text--center">
                <button class="btn  btn--small">
                    <span class="icon  icon--school"></span>
                    Button
                </button>
            </td>
            <td class="one-quarter  text--center">
                <button class="btn">
                    Button
                    <span class="icon  icon--navigate_next"></span>
                </button>
            </td>
            <td class="one-quarter  text--center">
                <button class="btn  btn--large">
                    <span class="icon  icon--whatshot"></span>
                    Button
                </button>
            </td>
            <td class="one-quarter  text--center">
                <button class="btn  btn--huge">
                    <span class="icon  icon--school"></span>
                    Button
                </button>
            </td>
        </tr>

        <tr class="milli muted">
            <td class="text--center hard--top">
                Small button
            </td>
            <td class="text--center hard--top">
                Default button
            </td>
            <td class="text--center hard--top">
                Large button
            </td>
            <td class="text--center hard--top">
                Huge button
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><button class="btn  btn--small">
    <span class="icon  icon--school"></span>
    Button
</button>

<button class="btn">
    <span class="icon  icon--palette"></span>
    Button
</button>

<button class="btn  btn--large">
    <span class="icon  icon--whatshot"></span>
    Button
</button>

<button class="btn  btn--huge">
    <span class="icon  icon--star"></span>
    Button
</button></script></code></pre>
