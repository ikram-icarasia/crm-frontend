

<h4 class="push-sm--bottom">Checkboxes</h4>

<p>
    We use custom styling for checkboxes. You should then wrap the whole thing in a <code class="code-plain">&lt;label&gt;</code> with the classes <code>.checkbox</code>.
</p>

<table class="pack--modifier">
    <thead>
        <tr>
            <th class="one-third">Modifier</th>
            <th class="two-third">Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="one-third">
                <code class="code-attr">:checked</code>
            </td>
            <td class="two-third">
                Checked state
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-attr">:disabled</code>
            </td>
            <td class="two-third">
                Disabled state
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-css">.checkbox--inline</code>
            </td>
            <td class="two-third">
                Inline checkbox
            </td>
        </tr>
    </tbody>
</table>

<div class="documentation-example hard">
    <table class="pack--column flush">
        <tbody>
            <tr>
                <td>
                    <p><code class="code-plain">default</code></p>
                    <label class="checkbox">
                        <input type="checkbox">
                        <span class="icon  icon--check"></span>
                        Checkbox
                    </label>
                </td>
                <td>
                    <p><code class="code-attr">:checked</code></p>
                    <label class="checkbox">
                        <input type="checkbox" checked="">
                        <span class="icon  icon--check"></span>
                        Checkbox
                    </label>
                </td>
                <td>
                    <p><code class="code-attr">:disabled</code></p>
                    <label class="checkbox">
                        <input type="checkbox" disabled="">
                        <span class="icon  icon--check"></span>
                        Checkbox
                    </label>
                </td>
            </tr>

            <tr>
                <td class="hard" colspan="3">
                    <hr class="flush">
                </td>
            </tr>

            <tr>
                <td colspan="3">
                    <p><code class="code-css">.checkbox-inline</code></p>
                    <label class="checkbox  checkbox--inline  push-md--right">
                        <input type="checkbox">
                        <span class="icon  icon--check"></span>
                        Checkbox
                    </label>

                    <label class="checkbox  checkbox--inline  push-md--right">
                        <input type="checkbox" checked="">
                        <span class="icon  icon--check"></span>
                        Checkbox
                    </label>

                    <label class="checkbox  checkbox--inline  push-md--right">
                        <input type="checkbox" disabled="">
                        <span class="icon  icon--check"></span>
                        Checkbox
                    </label>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<pre class="flush"><code class="language-markup  line-numbers"><script type="prism-html-markup"><label class="checkbox">
    <input type="checkbox">
    <span class="icon  icon--check"></span>
    Checkbox
</label>

<label class="checkbox">
    <input type="checkbox" checked="">
    <span class="icon  icon--check"></span>
    Checkbox
</label>

<label class="checkbox">
    <input type="checkbox" disabled="">
    <span class="icon  icon--check"></span>
    Checkbox
</label>

<label class="checkbox  checkbox--inline  push-md--right">
    <input type="checkbox">
    <span class="icon  icon--check"></span>
    Checkbox
</label>
<label class="checkbox  checkbox--inline  push-md--right">
    <input type="checkbox" checked="">
    <span class="icon  icon--check"></span>
    Checkbox
</label>
<label class="checkbox  checkbox--inline  push-md--right">
    <input type="checkbox" disabled="">
    <span class="icon  icon--check"></span>
    Checkbox
</label></script></code></pre>
