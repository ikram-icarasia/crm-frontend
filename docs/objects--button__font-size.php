<h4 class="push-sm--bottom">Button Font Size</h4>

<p>
    Button font-size modifiers.
</p>


<div class="documentation-example hard">
    <table class="pack--column flush">
        <tr>
            <td class="text--center">
                <button class="btn  btn--alpha">Button</button>
            </td>
            <td class="text--center">
                <button class="btn  btn--beta">Button</button>
            </td>
            <td class="text--center">
                <button class="btn  btn--gamma">Buttons</button>
            </td>
            <td class="text--center">
                <button class="btn">Button</button>
            </td>
        </tr>

        <tr class="milli muted">
            <td class="text--center hard-- hard--top">
                Alpha size
            </td>
            <td class="text--center hard-- hard--top">
                Beta size
            </td>
            <td class="text--center hard-- hard--top">
                Gamma size
            </td>
            <td class="text--center hard-- hard--top">
                Default
            </td>
        </tr>
    </table>
</div>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><button class="btn  btn--alpha">Button</button>
<button class="btn  btn--beta">Button</button>
<button class="btn  btn--gamma">Button</button>
<button class="btn">Button</button></script></code></pre>

<p>
    We also will be able to make the button to inherit parent font size by extending it with <code>.btn--natural</code>.
</p>

<pre><code class="language-markup  line-numbers"><script type="prism-html-markup"><button class="btn  btn--natural">Button</button></script></code></pre>
