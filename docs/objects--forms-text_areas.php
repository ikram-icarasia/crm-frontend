<h4 class="push-sm--bottom">Text Areas</h4>

<p>Text areas are similar to text inputs, but they are resizable.</p>

<table class="pack--modifier">
    <thead>
        <tr>
            <th class="one-third">Modifier</th>
            <th class="two-third">Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="one-third">
                <code class="code-attr">rows="3"</code>
            </td>
            <td class="two-third">
                Define numbers of text line of rows
            </td>
        </tr>
        <tr>
            <td class="one-third">
                <code class="code-attr">:disabled</code>
            </td>
            <td class="two-third">
                Disabled
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-attr">:readonly</code>
            </td>
            <td class="two-third">
                Read only
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-css">.input--success</code>
            </td>
            <td class="two-third">
                Success intent
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-css">.input--warning</code>
            </td>
            <td class="two-third">
                Warning intent
            </td>
        </tr>

        <tr>
            <td class="one-third">
                <code class="code-css">.input--error</code>
            </td>
            <td class="two-third">
                Error intent
            </td>
        </tr>
    </tbody>
</table>

<div class="documentation-example hard">
    <table class="pack--column flush">
        <tbody>
            <tr>
                <td>
                    <p><code class="code-plain">default</code></p>
                    <textarea rows="2" class="input"></textarea>
                </td>
                <td>
                    <p><code class="code-attr">:disabled</code></p>
                    <textarea rows="2" class="input" disabled=""></textarea>
                </td>
                <td>
                    <p><code class="code-attr">:readonly</code></p>
                    <textarea rows="2" class="input" readonly=""></textarea>
                </td>
            </tr>

            <tr>
                <td class="hard" colspan="3">
                    <hr class="flush">
                </td>
            </tr>

            <tr>
                <td>
                    <p><code class="code-class">.input--success</code></p>
                    <textarea rows="3" class="input  input--success"></textarea>
                </td>
                <td>
                    <p><code class="code-class">.input--warning</code></p>
                    <textarea rows="3" class="input  input--warning"></textarea>
                </td>
                <td>
                    <p><code class="code-class">.input--error</code></p>
                    <textarea rows="3" class="input  input--error"></textarea>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<pre class="flush"><code class="language-markup  line-numbers"><script type="prism-html-markup"><textarea rows="2" class="input"></textarea>
<textarea rows="2" class="input" disabled=""></textarea>
<textarea rows="2" class="input" readonly=""></textarea>
<textarea rows="3" class="input  input--success"></textarea>
<textarea rows="3" class="input  input--warning"></textarea>
<textarea rows="3" class="input  input--error"></textarea></script></code></pre>
