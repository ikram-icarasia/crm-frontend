$('.js-toggle-sidebar').click(function() {
    $('html').toggleClass('js-toggle-sidebar');
    $('.sidebar-backdrop').toggleClass('visuallyhidden');
});



$('.js-selectize').selectize({
    create: true,
    sortField: 'text'
});



$('.js-datepicker').pickadate();
$('.js-timepicker').pickatime();
