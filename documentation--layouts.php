<?php include 'views/templates/head.php'; ?>

    <h2 class="push-sm--bottom">Layouts</h2>

    <p style="font-size: 120%">Some common layouts.</p>

    <hr>

    <?php include 'docs/layout--widths.php'; ?>

    <hr>

    <?php include 'docs/layout--floats.php'; ?>

    <hr>

    <?php include 'docs/layout--grids.php'; ?>

    <hr>

    <?php include 'docs/layout--pack.php'; ?>

<?php include 'views/templates/foot.php'; ?>
