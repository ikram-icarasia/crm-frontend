<?php
// Include subtoolbar
$page_sidebar   = 'false';
$page_toolbar   = 'false';
$body_class     = 'access-login';

// Setup breadcrumbs
$breadcrumbs = array('Welcome');

include 'views/templates/head.php';
?>
<div class="content">
    <div class="container  container--sm  soft-lg--sides">
        <form class="panel  soft-lg">
            <h1 class="head-five  text--semibold">Login</h1>

            <div class="soft-lg  push-lg-sides  clearfix">
                <div class="form-group">
                    <label>Email Address</label>
                    <input type="text" class="input  push-xs--top  one-whole">
                </div>

                <div class="form-group  push-md--ends">
                    <label>Password</label>
                    <input type="password" class="input  push-xs--top  one-whole">
                </div>

                <label class="checkbox  push-lg--top  float--left">
                    <input type="checkbox" id="remember-me">
                    <span class="icon  icon--check"></span>
                    Remember me
                </label>

                <button class="btn  btn--positive  push-lg--top  float--right">Login</button>
            </div>
        </form>
    </div>
</div>
<?php include 'views/templates/foot.php'; ?>
