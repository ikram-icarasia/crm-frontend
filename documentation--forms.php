<?php include 'views/templates/head.php'; ?>

    <h2 class="push-sm--bottom">Forms</h2>

    <p style="font-size: 120%">Style individual form controls and utilize common layouts.</p>

    <hr class="muted">

    <?php include 'docs/objects--forms-checkboxes.php'; ?>

    <hr class="muted">

    <?php include 'docs/objects--forms-radios.php'; ?>

    <hr class="muted">

    <?php include 'docs/objects--forms-text_inputs.php'; ?>

    <hr class="muted">

    <?php include 'docs/objects--forms-text_areas.php'; ?>

<?php include 'views/templates/foot.php'; ?>
