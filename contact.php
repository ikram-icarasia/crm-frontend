
<?php
// Include subtoolbar
$toolbar_sub = 'views/contact/_subtoolbar-finder.php';

// Setup breadcrumbs
$breadcrumbs = array('Contact');

include 'views/templates/head.php';
?>

<main>
    <div class="panel">
        <table class="panel__table  table">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Country</th>
                    <th>Profile</th>
                    <th>Phone</th>
                    <th class="tight">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php for($count=1; $count<6; $count++) { ?>
                    <tr>
                        <td><a href="#">ikramhakimi</a></td>
                        <td>Ikram Hakimi</td>
                        <td>Mohd Zaki</td>
                        <td>ikram.hakimi@icarasia.com</td>
                        <td>Malaysia</td>
                        <td>Dealer</td>
                        <td>0123456789</td>
                        <td class="tight">
                            <a href="#" class="btn">Detail</a>
                            <a href="#" class="btn">Edit</a>
                            <a href="#" class="btn">Message</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="#">nut.charoontachatapot</a></td>
                        <td>Nut</td>
                        <td>Charoonchatapot</td>
                        <td>nut.charoonchatapot@icarasia.com</td>
                        <td>Thailand</td>
                        <td>Private</td>
                        <td>1101987654322</td>
                        <td class="tight">
                            <a href="#" class="btn">Detail</a>
                            <a href="#" class="btn">Edit</a>
                            <a href="#" class="btn">Message</a>
                        </td>
                    </tr>
                    <tr>
                        <td><a href="#">akbar.herlambang</a></td>
                        <td>Akbar</td>
                        <td>Herlambang</td>
                        <td>akbar.herlambang@icarasia.com</td>
                        <td>Indonesia</td>
                        <td>Private</td>
                        <td>8001128792</td>
                        <td class="tight">
                            <a href="#" class="btn">Detail</a>
                            <a href="#" class="btn">Edit</a>
                            <a href="#" class="btn">Message</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

    <?php include 'views/templates/pagination.php'; ?>
</main>

<?php include 'views/templates/foot.php'; ?>
