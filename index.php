<?php
// Include subtoolbar
$toolbar_sub = 'views/moderation/_subtoolbar-moderation-listing.php';

// Setup breadcrumbs
$breadcrumbs = array('Moderation', 'Listings');

$body_class         = '';
$page_toolbar       = 'true';
$page_sidebar       = 'true';
$toolbar_sub        = '';
$toolbar__button    = '';

include 'views/templates/head.php';
?>



<div class="container">
    <form class="panel">
        <section class="soft-md  hard--bottom">
            <div class="grid">
                <div class="grid__item  one-fifth  palm-one-whole  push-md--bottom">
                    <div class="fieldset">
                        <label class="text--muted">Textfield</label>
                        <input type="text" class="input  one-whole" placeholder="">
                    </div>
                </div>

                <div class="grid__item  one-fifth  palm-one-whole  push-md--bottom">
                    <div class="fieldset">
                        <label class="text--muted">Select</label>
                        <div class="selectize--disable-input  selectize--dropdown-right  selectize--dropdown-full">
                            <select id="select-beast" class="select  js-selectize">
                                <option>Option One</option>
                                <option>Option Two</option>
                                <option>Option Three</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="grid__item  one-fifth  palm-one-whole  push-md--bottom">
                    <div class="fieldset">
                        <label class="text--muted">Button Group</label>
                        <div class="btn-grouped  one-whole">
                            <a href="#" class="btn  one-third  text--normal">
                                One
                            </a>

                            <a href="#" class="btn  one-third  text--normal  text--negative">
                                Two
                            </a>

                            <a href="#" class="btn  one-third  text--normal">
                                Tri
                            </a>
                        </div>
                    </div>
                </div>

                <div class="grid__item  one-fifth  palm-one-whole  push-md--bottom">
                    <div class="fieldset">
                        <label class="text--muted">Button Radio</label>
                        <div class="btn-grouped  one-whole">
                            <input type="radio" id="radio-zero" name="radio" class="visuallyhidden" checked="">

                            <label for="radio-zero" class="btn  btn--radio  one-third  text--normal">
                                All
                            </label>

                            <input type="radio" id="radio-one" name="radio" class="visuallyhidden">

                            <label for="radio-one" class="btn  btn--radio  one-third  text--normal">
                                Online
                            </label>

                            <input type="radio" id="radio-two" name="radio" class="visuallyhidden">

                            <label for="radio-two" class="btn  btn--radio  one-third  text--normal">
                                Offline
                            </label>
                        </div>
                    </div>
                </div>

                <div class="grid__item  one-fifth  palm-one-whole  push-md--bottom">
                    <div class="fieldset">
                        <label class="text--muted">Checkbox Radio</label>
                        <label class="checkbox  float--left  push-md--right" for="check-one">
                            <input type="checkbox" id="check-one">
                            <span class="icon  icon--check"></span>
                            Checkbox
                        </label>

                        <label class="radio  float--left  push-md--right" for="radio-four">
                            <input type="radio" id="radio-four">
                            <span class="icon  icon--check"></span>
                            Radio
                        </label>
                    </div>
                </div>

                <div class="grid__item  one-fifth  palm-one-whole  push-md--bottom">
                    <div class="fieldset">
                        <label class="text--muted">Date Picker</label>
                        <input type="text" class="input  one-whole  js-datepicker" />
                    </div>
                </div>

                <div class="grid__item  one-fifth  palm-one-whole  push-md--bottom">
                    <div class="fieldset">
                        <label class="text--muted">Time Picker</label>
                        <input type="text" class="input  one-whole  js-timepicker" />
                    </div>
                </div>
            </div>
        </section>

        <section class="soft-md  pack  light--background">
            <div class="pack__item">
                24,821 records founded
            </div>
            <div class="pack__item  tight">
                <button class="btn">Reset</button>
                <button class="btn  btn--secondary">Apply Filter</button>
            </div>
        </section>
    </form>
    <div class="panel">
        <div class="panel__body">
            <div class="grid">
                <div class="grid__item  one-half">
                    <div class="alert">
                        SFL user has insufficient balance, listing sent to draft.
                    </div>

                    <div class="alert  pack">
                        <div class="pack__item  tight  soft-md--right">
                            <div class="alert__label">Info</div>
                        </div>
                        <div class="pack__item">
                            SFL user has insufficient balance, listing sent to draft.
                        </div>
                    </div>

                    <div class="alert  alert--success">
                        SFL user has insufficient balance, listing sent to draft.
                    </div>

                    <div class="alert  alert--success  pack">
                        <div class="pack__item  tight  soft-md--right">
                            <div class="alert__label">Success</div>
                        </div>
                        <div class="pack__item">
                            SFL user has insufficient balance, listing sent to draft.
                        </div>
                    </div>
                </div>
                <div class="grid__item  one-half">
                    <div class="alert  alert--warning">
                        SFL user has insufficient balance, listing sent to draft.
                    </div>

                    <div class="alert  alert--warning  pack">
                        <div class="pack__item  tight  soft-md--right">
                            <div class="alert__label">Warning</div>
                        </div>
                        <div class="pack__item">
                            SFL user has insufficient balance, listing sent to draft.
                        </div>
                    </div>

                    <div class="alert  alert--error">
                        SFL user has insufficient balance, listing sent to draft.
                    </div>

                    <div class="alert  alert--error  pack">
                        <div class="pack__item  tight  soft-md--right">
                            <div class="alert__label">Error</div>
                        </div>
                        <div class="pack__item">
                            SFL user has insufficient balance, listing sent to draft.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel">
        <table class="panel__table  pack  table--bordered  no--border">
            <thead>
                <tr>
                    <th colspan="2" class="hard--bottoms">
                        <div class="selectize--disable-input  selectize--dropdown-right  selectize--dropdown-full">

                            <select id="select-beast" class="select  js-selectize">
                                <option>Bulk Actions</option>
                                <option>Delete Selected</option>
                                <option>Move Selected</option>
                            </select>
                        </div>
                    </th>
                    <th colspan="4" class="hard--bottoms">
                        <input type="text" class="input" placeholder="Search by name" style="width: 30%">
                        <button class="btn  btn--secondary">Search</btn>
                    </th>
                    <th class="hard--bottoms">
                        <!-- <div class="selectize--link  selectize--transparent  selectize--tight  selectize--disable-input  selectize--dropdown-flexible  selectize--dropdown-right"> -->
                        <div class="selectize--disable-input  selectize--dropdown-right  selectize--dropdown-full">

                            <select id="select-beast" class="select  js-selectize">
                                <option>Best Match</option>
                                <option>Latest Updated</option>
                                <option>Price: Low to High</option>
                                <option>Price: High to Low</option>
                                <option>Year: New to Old</option>
                                <option>Year: Old to New</option>
                                <option>Mileage: Low to High</option>
                                <option>Mileage: High to Low</option>
                            </select>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th colspan="2">Username</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Country</th>
                    <th>Profile Type</th>
                    <th class="text--right">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php for($a=0; $a<5; $a++) { ?>
                <tr>
                    <td width="1%" class="soft-md--sides" style="border-right: 1px solid #EEE;">
                        <label class="checkbox  no--text  flush">
                            <input type="checkbox" class="js--check-parent">
                            <span class="icon  icon--check"></span>
                            &nbsp;
                        </label>
                    </td>

                    <td>
                        <a href="#">ikram.icarasia</a>
                    </td>

                    <td>
                        Ikram Hakimi bin Mohd Zaki
                    </td>

                    <td>
                        ikram.hakimi@icarasia.com
                    </td>

                    <td>
                        Malaysia
                    </td>

                    <td>
                        Sales Agent
                    </td>

                    <td class="tight" width="1%">
                        <a href="#" class="btn  btn--icon">
                            <i class="icon  icon--info"></i>
                        </a>

                        <a href="#" class="btn  btn--icon  text--negative">
                            <i class="icon  icon--delete"></i>
                        </a>

                        <a href="#" class="btn  btn--icon">
                            <i class="icon  icon--edit"></i>
                        </a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php include 'views/templates/foot.php'; ?>
