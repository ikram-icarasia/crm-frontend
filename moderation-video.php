
<?php
// Include subtoolbar
$toolbar_sub = 'views/moderation/_subtoolbar-moderation-listing.php';

// Setup breadcrumbs
$breadcrumbs = array('Moderation', 'Videos', 'Pre');

include 'views/templates/head.php';
?>

    <?php for($count=1; $count<4; $count++) { ?>
        <?php include 'views/moderation/video.php'; ?>
    <?php } ?>

    <?php include 'views/moderation/_modal-moderation-video-reject.php'; ?>

    <?php include 'views/templates/pagination.php'; ?>

<?php include 'views/templates/foot.php'; ?>
