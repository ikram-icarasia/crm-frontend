<?php $this->beginContent('@app/views/layouts/labs.php'); ?>
<?= $this->render('//labs/_template-sidebar') ?>
<?= $this->render('//labs/_template-start', ['breadcrumbs' => $this->params['breadcrumbs']]) ?>
<?= $content ?>
<?= $this->render('//labs/_template-close') ?>
<?php $this->endContent(); ?>
