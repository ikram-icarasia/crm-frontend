<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<?php
use \app\components\AdManager;
$brands = (isset($this->params['brands'])) ? $this->params['brands'] : [];
$make = (isset($this->params['make'])) ? $this->params['make'] : '';
?>
<div id="new-car-price-list" class="details  price-list  relative container push-md--bottom">
	<?=$this->render('//widgets/breadcrumb', ['breadcrumb' => $this->params['breadcrumb']])?>
	<div class="text--center  push-md--ends  no-print">
		<?= AdManager::showSlot("price_list_leader_board")?>
	</div>
	<?= $this->render('//widgets/cars/_makes', ['brands' => $brands, 'make'=>$make, 'ajax'=>true])?>
	<div id="pricing-models" class="pricing-models  relative">
		<?= $content ?>
	</div>
	<?=$this->render('//widgets/popup/calculator')?>
	<?=$this->render('//widgets/popup/variants', ['compareVariants' => $this->params['compareVariants']])?>
	<?=$this->render('//widgets/popup/showroom_compare', ['compareVariants' => $this->params['compareVariants']])?>
</div>
<?php $this->endContent(); ?>
