<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<?php
use app\components\UrlManager;
use app\components\AdManager;
?>
<nav class="subheader  subheader--finder  hard  transition--default  visuallyhidden--desk  visuallyhidden--desk-wide  js-listings__fixed-left  js-listings__fixed-left--sticky-top  js-part-facets">
	<?= $this->render('//widgets/facets/search_filter', [
		'searchParams' => $this->params['searchParams'],
		'makes' => $this->params['makes'],
		'locations' => $this->params['locations'],
		'total' => $this->params['total'],
		'vehicleType' => $this->params['vehicleType']
	]) ?>
</nav>

<?php if ($this->params['searchParams']->kurnia == 'true') : ?>
<div class="kurnia-blue-ribbon  relative">
	<div class="hero  tier--one  push-md--bottom">
		<?= $this->render('//widgets/slides/kurnia_slider') ?>
	</div>

	<div class="container  container--wide  hard  visuallyhidden--portable  js-show-on-load">
		<div class="classified-container  flexbox  tier--two">
			<div class="flexbox__item">
				<h1 class="headline  text--white">
					<span class="delta"><?=Yii::t('all', 'Welcome to')?></span>
					<span class="block  text--semibold"><?=Yii::t('all', 'Kurnia Blue Ribbon Special Deals')?></span>
				</h1>
				<a href="#classified-listings" class="btn  btn--primary  btn--large  js-scroll-to"><?= Yii::t('all', 'View all listings'); ?></a>
			</div>
		</div>
	</div>
</div>
<?php endif?>

<div id="classified-listings" class="listings  relative  <?php if ($this->params['searchParams']->kurnia == 'true') : ?>listings--kurnia<?php endif?>">
	<div class="container  container--listing  cf">

		<?=$this->render('//widgets/breadcrumb', ['breadcrumb' => $this->params['breadcrumb'], 'ajax' => true, 'class' => 'breadcrumb--listing  push-md--top  push-md--bottom  visuallyhidden--portable' ])?>

		<div class="grid">
			<nav class="listings__fixed-left  grid__item  palm-one-whole  visuallyhidden--portable  js-listings__fixed-left  js-listings__fixed-left--sticky-top  js-part-facets">
				<?= $this->render('//widgets/facets/facets', [
					'searchParams' => $this->params['searchParams'],
					'makes' => $this->params['makes'],
					'locations' => $this->params['locations'],
					'total' => $this->params['total']
				]) ?>
			</nav>
			<div class="listings__fixed-right  grid__item  palm-one-whole  float--right">
				<div class="grid">
					<section id="classified-listings-result" class="listings__section  grid__item  seven-tenths  portable-one-whole  palm-one-whole">
						<?= $content ?>
					</section>
					<aside class="listings__side  grid__item  three-tenths  palm-one-whole  portable-one-whole">
                        <?php if ($this->params['searchParams']->kurnia != 'true') :?>
                            <?=$this->render('//widgets/listings/_recent', ['recent' => $this->params['recentlyViewed'], 'vehicleType' =>  $this->params['vehicleType']])?>

                            <?=AdManager::showSlot('listings_half_page', 'ad_unit--side  push-md--ends  text--center')?>

                            <div class="listing__section--calculator  soft-md  push-md--bottom  fill--grey  js-tools">
                                <?=$this->render('//widgets/finance', ['price' => $this->params['loanPrice']])?>
                                <?= Yii::$app->params['countryCode'] == 'id' ? $this->render('//widgets/tools/_tnc') : '' ?>
                            </div>

                            <?=AdManager::showSlot('listings_mrec', 'ad_unit--side  push-md--bottom  text--center  js-ad-mrec')?>

                            <?=AdManager::showSlot('listings_portrait', 'ad_unit--side  push-md--bottom  text--center')?>

                            <hr class="visuallyhidden  js-ad-portrait" />

                            <?=$this->render('//widgets/cars/_reviews', ['cars' => $this->params['cars']])?>

                            <?=$this->render('//widgets/news/_sidebar', ['news' => $this->params['news']])?>
                        <?php else :
                            echo AdManager::showSlot('listings_mrec_kurnia', 'ad_unit--side  push-md--bottom  text--center');
                            echo AdManager::showSlot('listings_mrec_kurnia2', 'ad_unit--side  push-md--bottom  text--center');
                        endif ?>
					</aside>
				</div>
			</div>
		</div>
	</div>
</div>

<?=$this->render('//widgets/popup/dealer', ['vehicleType' => $this->params['searchParams']->vehicle_type])?>
<?=$this->render('//widgets/popup/compare', ['vehicleType' => $this->params['searchParams']->vehicle_type, 'compareListings' => $this->params['compareListings']])?>
<?=$this->render('//widgets/js/elastic_search_listings', ['searchParams' => $this->params['searchParams']])?>
<?php $this->endContent(); ?>
