<?php $this->beginPage();?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language?>">
<head>
<meta charset="<?=Yii::$app->charset?>"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<title><?=$this->title?></title>
<?php $this->head() ?>

</head>
<body class="theme  theme--<?=Yii::$app->params['countryCode']?>">

<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage(); ?>
