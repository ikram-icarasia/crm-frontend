<?php $this->beginPage();
list($language,) = Yii::$app->controller->language;
?>
<!DOCTYPE html>
<!--[if IE 8]><html lang="<?=$language?>" class="ie8"><![endif]-->
<!--[if !IE 8]><!--><html lang="<?=$language?>"><!--<![endif]-->
<head>
<meta charset="<?=Yii::$app->charset?>"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<title><?=$this->title?></title>
<?php
/*
* By using registerMetaTag we are getting issue with the HTML encoding for the value of the listing page
* i.e. <meta name="ga:csr:research:make_model" content="[{&#039;make&#039;:&#039;Mercedes-Benz&#039;,&#039;modelGroup&#039;:&#039;&#039;,&#039;model&#039;:&#039;CLA250 Shooting Brake&#039;,&#039;variant&#039;:&#039;&#039;}]">
*/
?>

<?php $this->head() ?>
<?php $this->registerJs('window.params = {};', \yii\web\View::POS_HEAD); ?>
<!--[if IE 8]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
<![endif]-->



<?php

// @REVIEW MYBP-3241 - OWA still uses desktop / mobile, do we still need this in future?
if (is_array(Yii::$app->params['owaTrackingSiteId']) && array_key_exists(Yii::$app->params['renderTheme'], Yii::$app->params['owaTrackingSiteId']) && Yii::$app->params['owaTrackingSiteId'][Yii::$app->params['renderTheme']]) :?>

<!-- Start Open Web Analytics Tracker -->
<script type="text/javascript">
    //<![CDATA[
    var owa_baseUrl = 'http://owa.icarasia.com/';
    var owa_cmds = owa_cmds || [];
    owa_cmds.push(['setSiteId', '<?=Yii::$app->params['owaTrackingSiteId'][Yii::$app->params['renderTheme']]?>']);
    // owa_cmds.push(['trackPageView']);
    // owa_cmds.push(['trackClicks']);
    owa_cmds.push(['setDomstreamSampleRate', <?=(Yii::$app->request->get("owsSampleRate") ? Yii::$app->request->get("owsSampleRate") : Yii::$app->params['owaSampleRatePercentage'])?>]);
    owa_cmds.push(['trackDomStream']);

    (function() {
        var _owa = document.createElement('script'); _owa.type = 'text/javascript'; _owa.async = true;
        owa_baseUrl = ('https:' == document.location.protocol ? window.owa_baseSecUrl || owa_baseUrl.replace(/http:/, 'https:') : owa_baseUrl );
        _owa.src = owa_baseUrl + 'modules/base/js/owa.tracker-combined-min.js';
        var _owa_s = document.getElementsByTagName('script')[0]; _owa_s.parentNode.insertBefore(_owa, _owa_s);
    }());
    //]]>
</script>
<!-- End Open Web Analytics Code -->
<?php endif; ?>
<script>(function() { var scriptTag = document.createElement('script'); scriptTag.type = 'text/javascript'; scriptTag.async = true; scriptTag.src = '<?= Yii::$app->params['commonCDNUrl']?>js/alephbet.min.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(scriptTag, s); })();</script>

<?php if (is_array(Yii::$app->params['piwikTracking']) && array_key_exists('siteId', Yii::$app->params['piwikTracking']) && Yii::$app->params['piwikTracking']['siteId']) :?>
<!-- Piwik -->
<script type="text/javascript">var _paq = _paq || []; _paq.push(['trackPageView']); _paq.push(['enableLinkTracking']); (function() { var u="<?=Yii::$app->params['piwikTracking']['url']?>"; _paq.push(['setTrackerUrl', u+'piwik.php']); _paq.push(['setSiteId', '<?=Yii::$app->params['piwikTracking']['siteId']?>']); _paq.push(['setCookieDomain', '<?=Yii::$app->params['piwikTracking']['domain']?>']); _paq.push(['setDomains', '<?=Yii::$app->params['piwikTracking']['domain']?>']); var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s); })();</script>
<noscript><p><img src="<?=Yii::$app->params['piwikTracking']['url']?>piwik.php?idsite=<?=Yii::$app->params['piwikTracking']['siteId']?>" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->
<?php endif; ?>
</head>
<body class="theme  theme--<?= Yii::$app->params['countryCode'] ?>  <?= @$this->params['bodyCSSClass'] ?>  cbp-spmenu-push">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=<?=Yii::$app->params['googleGTMCode']?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','<?=Yii::$app->params['googleGTMCode']?>');</script>
<!-- End Google Tag Manager -->

<?php $this->beginBody() ?>
<?= $this->render("//widgets/header") ?>
<?=$this->render("//widgets/smart_app_banner")?>
<main>
<?=$content?>
</main>
<?= $this->render("//widgets/footer") ?>
<?= $this->render("//widgets/chat") ?>
<?= $this->render("//widgets/popup/auth") ?>
<?php $this->endBody() ?>

</body>

</html>
<?php $this->endPage(); ?>
