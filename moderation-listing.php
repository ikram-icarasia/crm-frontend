
<?php
// Include subtoolbar
$toolbar_sub = 'views/moderation/_subtoolbar-moderation-listing.php';

// Setup breadcrumbs
$breadcrumbs = array('Moderation', 'Listings');

include 'views/templates/head.php';
?>

    <?php for($count=1; $count<4; $count++) { ?>
        <?php include 'views/moderation/item.php'; ?>
    <?php } ?>

    <?php include 'views/moderation/_modal-moderation-reject.php'; ?>

    <?php include 'views/templates/pagination.php'; ?>

<?php include 'views/templates/foot.php'; ?>
