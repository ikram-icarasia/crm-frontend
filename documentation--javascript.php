<?php include 'views/templates/head.php'; ?>

    <h2 class="push-sm--bottom">Javascript</h2>

    <p style="font-size: 120%">Custom Javascript components.</p>

    <hr>

    <?php include 'docs/components--dropdown.php'; ?>

    <hr class="rule rule--ornament">

    <?php include 'docs/components--tabs.php'; ?>

    <hr class="rule rule--ornament">

    <?php include 'docs/components--modal.php'; ?>

<?php include 'views/templates/foot.php'; ?>
