<?php include 'views/templates/head.php'; ?>
<section class="container  container--md">
    <h2 class="push-sm--bottom">Buttons</h2>

    <p style="font-size: 120%">Style individual form controls and utilize common layouts.</p>

    <hr>
    <?php include 'docs/objects--button__usage.php'; ?>

    <!-- <hr> -->
    <?php // include 'docs/objects--button__style.php'; ?>

    <hr>
    <?php include 'docs/objects--button__size.php'; ?>

    <hr>
    <?php include 'docs/objects--button__icon.php'; ?>

    <hr>
    <?php include 'docs/objects--button__options.php'; ?>

    <hr>
    <?php include 'docs/objects--button__disabled.php'; ?>

    <!-- <hr> -->
    <?php // include 'docs/objects--button__full.php'; ?>
</section>
<?php include 'views/templates/foot.php'; ?>
