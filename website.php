<html lang="en" class="<?php echo $screen_size; ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <link type="text/css" href="styles/website.css" rel="stylesheet">
</head>
<body>
    <div class="header">
        <div class="container">
            <h2 class="logo  head--four  float--left  flush"><a href="#">Engine</a></h2>
            <ul class="menu  float--left">
                <li><a href="#">Home</a></li>
                <li><a href="#">Features</a></li>
                <li><a href="#">Company</a></li>
                <li><a href="#">Help</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="#">Pricing</a></li>
                <li><a href="#">Support</a></li>
                <li><a href="#" class="btn  valign--top">Log In</a></li>
            </ul>
            <div class="access  table  no--wrap  float--right">
                <div class="table-cell">Have an account?</div>
                <div class="table-cell">
                    <a href="#" class="btn  valign--top">Log In</a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
