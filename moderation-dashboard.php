
<?php

// Setup breadcrumbs
$breadcrumbs = array('Moderation', 'Dashboard');

include 'views/templates/head.php';
?>

<div class="container  container--md">
    <div class="grid">
        <div class="grid__item  one-half">
            <div class="island">
                <h6 class="soft-md  flush">Listing Submitted</h6>
                <div class="pack  soft-md--sides  soft-sm--ends">
                    <div class="pack__item">Assigned to you</div>
                    <div class="pack__item  tight">
                        <span class="text--muted">None</span>
                    </div>
                </div>
                <hr class="rule  rule--light  push-sm--ends  push-md--left">
                <div class="pack  soft-md--sides  soft-sm--ends  push-sm--bottom">
                    <div class="pack__item">All</div>
                    <div class="pack__item  tight">381</div>
                </div>
            </div>
        </div>
        <div class="grid__item  one-half">
            <div class="island">
                <h6 class="soft-md  flush">Published Post Moderation</h6>
                <div class="pack  soft-md--sides  soft-sm--ends">
                    <div class="pack__item">Assigned to you</div>
                    <div class="pack__item  tight">
                        <span class="text--positive">408</span>
                    </div>
                </div>
                <hr class="rule  rule--light  push-sm--ends  push-md--left">
                <div class="pack  soft-md--sides  soft-sm--ends  push-sm--bottom">
                    <div class="pack__item">All</div>
                    <div class="pack__item  tight">2,411</div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel">
        <table class="panel__table">
            <thead>
                <tr>
                    <th colspan="3">
                        <h6 class="flush">Listings Assignment</h6>
                    </th>
                </tr>
                <tr>
                    <th>Moderator</th>
                    <th>Pending Moderation</th>
                    <th>Pending Post Moderation</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>Mohd Zurkarnaim</td>
                    <td>15</td>
                    <td>193</td>
                </tr>

                <tr>
                    <td>Nasrullah Haikal</td>
                    <td>105</td>
                    <td>188</td>
                </tr>

                <tr>
                    <td>Govinraaj Arumugam</td>
                    <td>20</td>
                    <td>144</td>
                </tr>

                <tr>
                    <td>Magelelind Peter</td>
                    <td>119</td>
                    <td>305</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<?php include 'views/templates/foot.php'; ?>
